﻿KAZ_ARMY_DIVISIONS = {
	name = "Army Divisions"

	for_countries = { KAZ }

	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" "Mech_Inf_Bat" "Arm_Inf_Bat" }

	link_numbering_with = { "KAZ_ARMOURED_DIVISIONS" }

	fallback_name = "%d Aşıqtar dïvïzïyası"

	ordered = {
		645 = { "%d Artilleriyalyk polk" }
		962 = { "%d-shі Kep rettіk zymyran artilleriyalyk polkі" }
	}
}

KAZ_ARMY_BRIGADES = {
	name = "Army Brigades"
	for_countries = { KAZ }

	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" "Mech_Inf_Bat" "Arm_Inf_Bat" }

	fallback_name = "%d Jayaw äsker brïgadası"

	ordered = {
		57 = { "%d-shі Zheke aue shabuyldau brigadasy" }
		272 = { "%d Zenittіk-zymyrandyk brigada" }
		56 = { "%d Radiotekhnikalyk brigada" }
		44 = { "%d-shі Raketalyk brigada" }
	}
}

KAZ_MECHANIZED_DIVISIONS = {
	name = "Mechanized Division"
	for_countries = { KAZ }

	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" "Mech_Inf_Bat" "Arm_Inf_Bat" }

	fallback_name = "%d Mexanïkalandırılğan bölim"

	ordered = {
		155 = { "%d Motoatkyshtar diviziyasy" }
		68 = { "%d-shі Motoatkyshtar diviziyasy" }
		203 = { "%d Motoatkyshtar diviziyasy" }
		80 = { "%d-shі Gvardiyalyk oku motoatkyshtar diviziyasy" }
	}
}

KAZ_ARMOURED_DIVISIONS = {
	name = "Armoured Divisions"

	for_countries = { KAZ }

	division_types = { "armor_Bat" }

	link_numbering_with = { "KAZ_ARMY_DIVISIONS" }

	fallback_name = "%d Tank dïvïzïyası"

	ordered = {
		78 = { "%d-shі Tank diviziyasy" }
	}
}


KAZ_AIR_CAV_BRIGADES = {
	name = "Air Assault Brigades"

	for_countries = { KAZ }

	division_types = { "L_Air_assault_Bat" "Arm_Air_assault_Bat" "L_Air_Inf_Bat" "Mot_Air_Inf_Bat"  }

	fallback_name = "%d Desanttıq brïgadalar"

}

KAZ_MAR_BRIGADES = {
	name = "Amphibious Rapid Deployment Brigades"

	for_countries = { KAZ }

	division_types = { "L_Marine_Bat" "Mot_Marine_Bat" "Mech_Marine_Bat" "Arm_Marine_Bat" }

	fallback_name = "%d Amfïbïya jedel järdem brïgadası"
}

KAZ_SPEC_FORCES_BRIGADES = {
	name = "Special Forces Groups"

	for_countries = { KAZ }

	division_types = { "Special_Forces" }

	fallback_name = "%d Arnayı küşterdiñ toptarı"
}

KAZ_MIL_BRIGADES = {
	name = "Regional Defence Brigades"

	for_countries = { KAZ }

	division_types = { "Militia_Bat" "Mot_Militia_Bat" }

	fallback_name = "%d Aymaqtıq qorğanıs brïgadaları"
}