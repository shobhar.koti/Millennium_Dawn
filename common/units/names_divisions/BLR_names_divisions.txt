﻿BLR_ARM_01 = {
	name = "Mechanized Brigade"

	can_use = {
		OR = {
			has_western_aligned_government = yes
			has_nationalist_goverment = yes
		}
	}

	for_countries = { BLR }

	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" "Mech_Inf_Bat" "Arm_Inf_Bat" }

	fallback_name = "%dth Mechanized Brigade"

	ordered = {
		1 = { "%dst Mechanized 'Kastusya Kalinouskaga' Brigade" }
		2 = { "%dnd Mechanized 'Tadeusza Kostushk' Brigade" }
		3 = { "%drd Mechanized 'Minskaya' Brigade" }
		4 = { "%dth Mechanized 'Polotskaya' Brigade" }
		5 = { "%dth Mechanized 'Pinskaya' Brigade" }
		6 = { "%dth Mechanized 'Bobruyskaya' Brigade" }
		7 = { "%dth Mechanized 'Litvin' Brigade" }
		8 = { "%dth Mechanized 'Mogilevskaya' Brigade" }
	}
}

BLR_ARM_02 = {
	name = "Tank Brigade"

	can_use = {
		OR = {
			has_western_aligned_government = yes
			has_nationalist_goverment = yes
		}
	}

	for_countries = { BLR }

	division_types = { "Arm_Inf_Bat" "armor_Bat" }

	fallback_name = "%dth Tank Brigade"

	ordered = {
		1 = { "%dst Tank 'Pahonya' Brigade" }
		2 = { "%dnd Tank 'Respublikanskaya' Brigade" }
		3 = { "%drd Tank 'Brestskaya' Brigade" }
	}
}

BLR_INF_01 = {
	name = "Assault Brigade"

	can_use = {
		OR = {
			has_western_aligned_government = yes
			has_nationalist_goverment = yes
		}
	}

	for_countries = { BLR }

	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" "Special_Forces" }

	fallback_name = "%dth Assault Brigade"

	ordered = {
		1 = { "%dst Assault 'Lva Sapegi' Brigade" }
		2 = { "%dnd Assault 'Vitebskaya' Brigade" }
		3 = { "%drd Assault 'Gomelskaya' Brigade" }
	}
}

BLR_INF_02 = {
	name = "Special Purpose Brigade"

	can_use = {
		OR = {
			has_western_aligned_government = yes
			has_nationalist_goverment = yes
		}
	}

	for_countries = { BLR }

	division_types = { "L_Inf_Bat" "Special_Forces" }

	fallback_name = "%dth Special Purpose Brigade"

	ordered = {
		1 = { "%dst Assault 'Almaz' Brigade" }
		2 = { "%dnd Assault 'Volat' Brigade" }
		3 = { "%drd Assault 'Prezidentskaya' Brigade" }
	}
}