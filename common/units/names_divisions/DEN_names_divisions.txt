﻿#Author: Borgers
DEN_MIL = {
	name = "Security Units"

	for_countries = {
		DEN
	}


	division_types = { "Militia_Bat" "Mot_Militia_Bat" }

	fallback_name = "%d. Security Unit"

	ordered = {
		1 = { "%d. Territorial Region" }
		2 = { "%d. Territorial Region" }
	}

}

DEN_INF = {
	name = "Infantry Brigades"

	for_countries = {
		DEN
	}


	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" }

	link_numbering_with = { DEN_MECH }

	fallback_name = "%d. Infanteri Brigade"

	ordered = {
		1 = { "%d. Infanteri Brigade" }
	}

}

DEN_INF_DIV = {
	name = "Infantry Divisions"

	for_countries = {
		DEN
	}



	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" }

	link_numbering_with = { DEN_MECH_DIV }

	fallback_name = "%d. Infanteri Division"

	ordered = {
		1 = { "%d. Infanteri Division" }
	}

}

DEN_MECH = {
	name = "Mechanised Brigades"

	for_countries = {
		DEN
	}



	division_types = { "Mech_Inf_Bat" "Arm_Inf_Bat" }

	link_numbering_with = { DEN_INF }

	fallback_name = "%d. Mekaniseret Brigade"

	ordered = {
		1 = { "1. Jydske Brigade" }
		2 = { "1. Sjællandske Brigade" }
		3 = { "3. Jydske Brigade" }
		4 = { "Den Danske Internationale Brigade" }
	}

}

DEN_MECH_DIV = {
	name = "Mechanised Divisions"

	for_countries = {
		DEN
	}



	division_types = { "Mech_Inf_Bat" "Arm_Inf_Bat" }

	link_numbering_with = { DEN_INF_DIV }

	fallback_name = "%d. Mekaniseret Division"

	ordered = {
		1 = { "1. Mekaniseret Division" }
	}

}

DEN_AIR_I = {
	name = "Airborne Brigades"

	for_countries = {
		DEN
	}



	division_types = { "L_Air_Inf_Bat" "Mot_Air_Inf_Bat" "Mech_Air_Inf_Bat" "Arm_Air_Inf_Bat" }

	fallback_name = "%d. Luftbåren Brigade"

	ordered = {
		1 = { "1. Luftbåren Brigade" }
	}

}

DEN_AIR_II = {
	name = "Airlanding Brigades"

	for_countries = {
		DEN
	}



	division_types = { "L_Air_Inf_Bat" "Mot_Air_Inf_Bat" "Mech_Air_Inf_Bat" "Arm_Air_Inf_Bat" }

	fallback_name = "%d. Faldskærmssoldater Brigade"

	ordered = {
		1 = { "1. Faldskærmssoldater Brigade" }
	}

}

DEN_AIR_DIV = {
	name = "Airborne Divisions"

	for_countries = {
		DEN
	}

	division_types = { "L_Air_Inf_Bat" "Mot_Air_Inf_Bat" "Mech_Air_Inf_Bat" "Arm_Air_Inf_Bat" }

	fallback_name = "%d. Luftbåren Division"

	ordered = {
		1 = { "1. Luftbåren Division" }
	}

}

DEN_AIR_CAV = {
	name = "Air Assault Brigades"

	for_countries = {
		DEN
	}

	division_types = { "L_Air_assault_Bat" "Arm_Air_assault_Bat" }

	fallback_name = "%d. Luftangrebs Brigade"

	ordered = {
		1 = { "1. Luftangrebs Brigade" }
	}

}

DEN_AIR_CAV_DIV = {
	name = "Air Assault Divisions"

	for_countries = {
		DEN
	}

	division_types = { "L_Air_assault_Bat" "Arm_Air_assault_Bat" }

	fallback_name = "%d. 1. Luftangrebs Division"

	ordered = {
		1 = { "1. Luftangrebs Division" }
	}

}

DEN_ARM = {
	name = "Armoured Brigades"

	for_countries = {
		DEN
	}

	division_types = { "armor_Bat" "L_arm_Bat" }

	fallback_name = "%d. Panser Brigade"

	ordered = {
		1 = { "1. Panser Brigade" }
	}

}

DEN_ARM_DIV = {
	name = "Armoured Divisions"

	for_countries = {
		DEN
	}

	division_types = { "armor_Bat" "L_arm_Bat" }

	fallback_name = "%dd.Armoured Division"

	ordered = {
		1 = { "1. Panser Division" }
	}

}

DEN_SPEC = {
	name = "Commando Units"

	for_countries = {
		DEN
	}

	division_types = { "Special_Forces" "L_Marine_Bat" "Mot_Marine_Bat" "Mech_Marine_Bat" "Arm_Marine_Bat" }

	fallback_name = "%d. Specialstyrkerkorpset"

	ordered = {
		1 = { "Frømandskorpset" }
		2 = { "Jægerkorpset" }
		3 = { "Slædepatruljen Sirius" }
	}
}
