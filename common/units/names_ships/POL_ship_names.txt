﻿##### Poland NAME LISTS #####

POL_HELICOPTER_CARRIERS_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_HELICOPTER_CARRIERS
	for_countries = { POL }
	type = ship
	ship_types = {
		helicopter_operator
	}
	prefix = "ORP "
	fallback_name = "LHA-%d"

	unique = {
		"Orzeł" "Błyskawica" "Grom" "Burza" "Ogien"
	}
}

### REGULAR DESTROYER NAMES###
POL_DD_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_DESTROYERS

	for_countries = { POL }

	type = ship
	ship_types = { stealth_destroyer destroyer }

	prefix = "ORP "
	fallback_name = "DDG-%d"

	unique = {
		"Nawałnica" "Wicher" "Huragan" "Piorun" "Gromada" "Strzała" "Odwet" "Wilczek" "Bór" "Wizna" "Warta" "Krak" "Czarnik" "Biskupin" "Orkan" "Smok" "Sokół" "Gawron" "Sowa" "Kruk" "Czapla" "Jarząb" "Mewa" "Sęp" "Jaskółka" "Kania" "Jastrząb" "Rybitwa" "Dzik" "Jeleń"
	}
}

POL_FRIGATES_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_FRIGATES
	for_countries = { POL }
	type = ship
	ship_types = {
		frigate
	}
	prefix = "ORP "
	fallback_name = "FFG-%d"

	unique = {
		"Żubr" "Lis" "Kot" "Sarna" "Kozioł" "Żyrafa" "Pies" "Karp" "Lin" "Kiełb" "Barszcz" "Żurek" "Pieróg" "Golonka" "Bigos" "Kurczak" "Placki" "Szarlotka" "Kluski" "Makowiec" "Chłodnik" "Zapiekanka" "Kiełbasa" "Pączki" "Sernik" "Krokiety" "Pyzy" "Kompot" "Gęś" "Szczupak" "Sielawa" "Leszcz" "Łosoś" "Pstrąg" "Sum" "Ryba" "Węgorz" "Dorsz" "Makrela" "Śledź" "Krewetki" "Homar" "Ośmiornica" "Kraba" "Małż" "Skrzypce" "Trąbka" "Gitara" "Saksofon" "Bęben" "Harfa" "Flet" "Klawesyn" "Wiolonczela" "Puzon" "Kontrabas" "Bębenek" "Skrzynia" "Walizka" "Mapa" "Kompas" "Książka" "Długopis" "Ołówek" "Papier" "Kartka" "Zeszyt"
	}
}

### CORVETTE NAMES ###
POL_CORVETTE_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_CORVETTE

	for_countries = { POL }
	prefix = "ORP "
	type = ship
	ship_types = { corvette }
	fallback_name = "KKG-%d"

	unique = {
		"Linijka" "Guma" "Nożyczki" "Klej" "Plecak" "Podręcznik" "Zegar" "Kalkulator" "Rower" "Samochód" "Pociąg" "Samolot" "Statek" "Autobus" "Tramwaj" "Motor" "Hulajnoga" "Staw" "Rzeka" "Góra" "Las" "Pole" "Wzgórze" "Wyspa" "Plaża" "Klif" "Jaskinia" "Dolina" "Zatoka" "Pustynia" "Skała" "Lodowiec" "Wodospad" "Sztorm" "Deszcz" "Chmury" "Słońce" "Księżyc" "Gwiazdy" "Zorza" "Mgła" "Tęcza" "Wiatr" "Dzień" "Noc" "Skrzydło" "Łopata" "Kij" "Sztylet" "Topór" "Miecz" "Kusza" "Łuk" "Zbroja" "Pancerz" "Hełm" "Tarcza" "Kamień" "Srebro" "Złoto" "Bursztyn" "Szmaragd" "Rubin" "Diament"
	}
}

POL_SUBMARINES_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_SUBMARINES
	for_countries = { POL }
	prefix = "ORP "
	type = ship
	ship_types = {
		attack_submarine missile_submarine
	}
	fallback_name = "SS-%d"
}