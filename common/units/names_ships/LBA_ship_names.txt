﻿LBA_CV_HISTORICAL = {
	name = NAME_THEME_MODERN_CARRIERS

	for_countries = { LBA }

	type = ship
	ship_types = { helicopter_operator carrier }

	prefix = ""
	fallback_name = "Hamilat Tayirat %d"
	
	unique = {
		"Gaddafi" "Battaros" "Ahmed Karamanli" "Yusuf Karamanli" "Idris" "Umar Mihayshi" 
	}
}

LBA_BB_HISTORICAL = {
	name = NAME_THEME_RIVER_MOUNTAIN

	for_countries = { LBA }

	type = ship
	ship_types = { battleship }

	prefix = ""
	fallback_name = "Safinat Harbia %d"
	
	unique = {
		"Bikku Bitti" "Jabal Arkanu" "Harouj" "Jebel Akhdar" "Jebel Sherif" "Wav an Namus"
	}
}

LBA_CA_HISTORICAL = {
	name = NAME_THEME_MODERN_CRUISERS

	for_countries = { LBA }

	type = ship
	ship_types = { cruiser battle_cruiser }

	prefix = ""
	fallback_name = "Tirad %d"
	
	unique = {
		"Tripoli" "Benghazi" "Misrata" "Zawiya" "Bayda" "Gharyan" "Tobruk" "Ajdabiya" "Zliten" "Derna" "Sabha" "Khoms" "Sabratha"
	}
}

LBA_DD_HISTORICAL = {
	name = NAME_THEME_MODERN_DESTROYERS

	for_countries = { LBA }

	type = ship
	ship_types = { destroyer stealth_destroyer }

	prefix = ""
	fallback_name = "Mudamir %d"
	
	unique = {
		"Sulaiman al-Barouni" "Omar al-Mukhtar" "Sayyid Ahmed Sharif es Senussi" "Ramadan Sewehli" "Omar Shegewi"
	}
}

LBA_FRIGATE_HISTORICAL = {
	name = NAME_THEME_MODERN_FRIGATES

	for_countries = { LBA }

	type = ship
	ship_types = { frigate }

	prefix = ""
	fallback_name = "Firqata %d"
	
	unique = {
		"Dat Assawari" "Ibn Ouf" "Ibn Haritha" "Tobruk" "Susa" "Sirte" "Sebha" "Al Katum" "Al O'Work" "Al Rwae" "Al Baida"
		"Al Nabhaa" "Al Fikar" "Al Safhaa" "Al Zakab" "Al Zuara" "Garian" "Khawlan" "Merawa" "Sabratha" "Ar Rakib" "Farwa"
		"Benina" "Misurata" "Akrama" "Homs"
	}
}

LBA_CORVETTE_HISTORICAL = {
	name = NAME_THEME_MODERN_CORVETTES

	for_countries = { LBA }

	type = ship
	ship_types = { corvette }

	prefix = ""
	fallback_name = "Kurfit %d"
	
	unique = {
		"Al Hani" "Al Ghardabia" "Tariq Ibn Ziyad" "Ain Al Gazala" "Ain Zaara" "Ain Zaquit" "Shafak" "Al Tadjier" "Al Tougour"
		"Al Kalij" "Al Hudud" "Sharara" "Shehab" "Wahag" "Waheed" "Shouaiai" "Shoula" "Bark" "Rad" "Laheeb" "Zuara" "Brak"
		"Al'Isar" "Al Tayyer" "Ras al Hamman" "Ras al Falluga" "Ras al Oula" "Ras al Dawar" "Ras Massad" "Ras al Hani"
	}
}

LBA_SUBMARINE_HISTORICAL = {
	name = NAME_THEME_MODERN_SUBMARINES

	for_countries = { LBA }

	type = ship
	ship_types = { missile_submarine attack_submarine }

	prefix = ""
	fallback_name = "Ghawaasa %d"
	
	unique = {
		"Al Badr" "Al Fatah" "Al Ahad" "Al Matrega" "Al Khyber" "Al Hunyan" "Ras el Hillel" "El Kobayat" "Ibn al Idrissi"
		"Ibn Marwhan" "Beir Grassa" "Beir Gzir" "Beir Gtifa" "Beir Glulud" "Beir Algandula" "Beir Ktitat" "Beir Alkarim"
		"Beir Alkardmen" "Beir Alkur" "Beir Alkuesat"
	}
}
