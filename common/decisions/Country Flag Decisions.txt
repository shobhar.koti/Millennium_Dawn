different_country_flags_category = {

	#Generic
	Reset_flag = {
		icon = GFX_decision_generic_form_nation
		cost = 0
		ai_will_do = { factor = 1 }
		allowed = {
			NOT = {
				original_tag = USA
				original_tag = BOS
				original_tag = SMA
				original_tag = RAJ
				original_tag = BUL
				original_tag = CUB
				original_tag = POL
			}

		}

		visible = {
			has_country_flag = dynamic_flag
			has_elections = yes
			if = {
				limit = { has_country_flag = is_civil_war_faction }
				meta_trigger = {
					text = {
						NOT = {
							if = {
								limit = { has_country_flag = dynamic_flag_new }
								has_cosmetic_tag = [ROOTTAG]_NEW
							}
							else = {
								has_cosmetic_tag = [ROOTTAG]
							}
							AND = {
								has_civil_war = yes
								OR = {
									NOT = { tag = [ROOTTAG] }
									has_cosmetic_tag = [ROOTTAG]_REB
									has_cosmetic_tag = [ROOTTAG]_REB_S
								}
							}
						}
					}
					ROOTTAG = "[?civil_war_original_tag.GetTag]"
				}
			}
			else = {
				meta_trigger = {
					text = {
						NOT = {
							if = {
								limit = { has_country_flag = dynamic_flag_new }
								has_cosmetic_tag = [ROOTTAG]_NEW
							}
							else = {
								has_cosmetic_tag = [ROOTTAG]
							}
							AND = {
								has_civil_war = yes
								OR = {
									NOT = { tag = [ROOTTAG] }
									has_cosmetic_tag = [ROOTTAG]_REB
									has_cosmetic_tag = [ROOTTAG]_REB_S
								}
							}
						}
					}
					ROOTTAG = "[?original_tag.GetTag]"
				}
			}
			if = {
				limit = { tag = PER }
				NOT = {
					OR = {
						emerging_hardline_shiite_are_in_power = yes
						emerging_moderate_shiite_are_in_power = yes
					}
				}
			}
			if = {
				limit = { tag = AZE }
				NOT = {
					OR = {
						emerging_hardline_shiite_are_in_power = yes
						emerging_moderate_shiite_are_in_power = yes
					}
				}
			}
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision Reset_flag"

			if = {
				limit = { has_country_flag = is_civil_war_faction }
				meta_effect = {
					text = {
						if = {
							limit = { has_country_flag = dynamic_flag_new }
							set_cosmetic_tag = [ROOTTAG]_NEW
						}
						else = {
							set_cosmetic_tag = [ROOTTAG]
						}
					}
					ROOTTAG = "[?civil_war_original_tag.GetTag]"
				}
			}
			else = {
				meta_effect = {
					text = {
						if = {
							limit = { has_country_flag = dynamic_flag_new }
							set_cosmetic_tag = [ROOTTAG]_NEW
						}
						else = {
							set_cosmetic_tag = [ROOTTAG]
						}
					}
					ROOTTAG = "[?original_tag.GetTag]"
				}
				custom_effect_tooltip = tooltip_change_flag
			}
		}
	}

	Autocracy_flag = {
		icon = GFX_decision_generic_form_nation
		cost = 0
		ai_will_do = { factor = 1 }
		allowed = {
			NOT = {
				original_tag = BOS
				original_tag = SMA
				original_tag = RAJ
				original_tag = BUL
			}
		}
		visible = {
			NOT = { original_tag = BOS }
			NOT = { original_tag = RAJ }
			NOT = { original_tag = BUL }
			NOT = {
				has_cosmetic_tag = PER_IRANIAN_EMPIRE
				has_cosmetic_tag = PER_interim_government
				has_cosmetic_tag = CZR_union
				has_cosmetic_tag = GER_AUTH_S_communism
				has_cosmetic_tag = GER_com_reb_tech
				has_cosmetic_tag = EGY_COPTIC
			}
			has_country_flag = dynamic_flag
			has_elections = no
			OR = {
				is_in_array = { array = ruling_party value = 0 }
				is_in_array = { array = ruling_party value = 7 }
				is_in_array = { array = ruling_party value = 13 }
				is_in_array = { array = ruling_party value = 22 }

				#Normally democratic party is AUTH
				AND = {
					NOT = {
						has_civil_war = yes
						if = {
							limit = { has_country_flag = is_civil_war_faction }
							meta_trigger = {
								text = {
									has_cosmetic_tag = [ROOTTAG]
									has_cosmetic_tag = [ROOTTAG]_NEW
								}
								ROOTTAG = "[?civil_war_original_tag.GetTag]"
							}
						}
						else = {
							meta_trigger = {
								text = {
									has_cosmetic_tag = [ROOTTAG]
									has_cosmetic_tag = [ROOTTAG]_NEW
								}
								ROOTTAG = "[?original_tag.GetTag]"
							}
						}
					}
					OR = {
						is_in_array = { array = ruling_party value = 1 }
						is_in_array = { array = ruling_party value = 2 }
						is_in_array = { array = ruling_party value = 3 }
						is_in_array = { array = ruling_party value = 5 }
						is_in_array = { array = ruling_party value = 6 }
						is_in_array = { array = ruling_party value = 12 }
						is_in_array = { array = ruling_party value = 14 }
						is_in_array = { array = ruling_party value = 15 }
						is_in_array = { array = ruling_party value = 16 }
						is_in_array = { array = ruling_party value = 17 }
						is_in_array = { array = ruling_party value = 18 }
						is_in_array = { array = ruling_party value = 20 }
					}
				}
			}
			if = {
				limit = { has_country_flag = is_civil_war_faction }
				meta_trigger = {
					text = {
						NOT = {
							has_cosmetic_tag = [ROOTTAG]_AUTH
							AND = {
								has_civil_war = yes
								OR = {
									is_in_array = { array = ruling_party value = 22 }
									AND = {
										NOT = { tag = [ROOTTAG] }
										[ROOTTAG] = {
											OR = {
												is_in_array = { array = ruling_party value = 0 }
												is_in_array = { array = ruling_party value = 7 }
												is_in_array = { array = ruling_party value = 13 }
											}
										}
									}
								}
							}
						}
					}
					ROOTTAG = "[?civil_war_original_tag.GetTag]"
				}
			}

			else = {
				meta_trigger = {
					text = {
						NOT = {
							has_cosmetic_tag = [ROOTTAG]_AUTH
							AND = {
								has_civil_war = yes
								OR = {
									is_in_array = { array = ruling_party value = 22 }
									AND = {
										NOT = { tag = [ROOTTAG] }
										[ROOTTAG] = {
											OR = {
												is_in_array = { array = ruling_party value = 0 }
												is_in_array = { array = ruling_party value = 7 }
												is_in_array = { array = ruling_party value = 13 }
											}
										}
									}
								}
							}
						}
					}
					ROOTTAG = "[?original_tag.GetTag]"
				}
			}
			if = {
				limit = { tag = PER }
				NOT = {
					OR = {
						emerging_hardline_shiite_are_in_power = yes
						emerging_moderate_shiite_are_in_power = yes
					}
				}
			}
			if = {
				limit = { tag = AZE }
				NOT = {
					OR = {
						emerging_hardline_shiite_are_in_power = yes
						emerging_moderate_shiite_are_in_power = yes
					}
				}
			}
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision Autocracy_flag"

			if = {
				limit = { has_country_flag = is_civil_war_faction }
				meta_effect = {
					text = { set_cosmetic_tag = [ROOTTAG]_AUTH }
					ROOTTAG = "[?civil_war_original_tag.GetTag]"
				}
			}
			else = {
				meta_effect = {
					text = { set_cosmetic_tag = [ROOTTAG]_AUTH }
					ROOTTAG = "[?original_tag.GetTag]"
				}
				custom_effect_tooltip = tooltip_change_flag
			}
		}
	}

	Special_flag = {
		icon = GFX_decision_generic_form_nation
		cost = 0
		ai_will_do = { factor = 1 }
		allowed = {
			NOT = {
				original_tag = BOS
				original_tag = SMA
				original_tag = RAJ
				original_tag = BUL
			}
		}
		visible = {
			has_country_flag = dynamic_flag
			has_elections = no
			NOT = {
				has_cosmetic_tag = PER_IRANIAN_EMPIRE
				has_cosmetic_tag = PER_interim_government
				has_cosmetic_tag = CZR_union
				has_cosmetic_tag = GER_AUTH_S_communism
				has_cosmetic_tag = GER_com_reb_tech
				has_cosmetic_tag = EGY_COPTIC
			}
			if = {
				limit = { has_country_flag = is_civil_war_faction }
				meta_trigger = {
					text = {
						NOT = { has_cosmetic_tag = [ROOTTAG]_AUTH_S }
						OR = {
							is_in_array = { array = ruling_party value = 10 }
							is_in_array = { array = ruling_party value = 23 }
							AND = {
								OR = {
									is_in_array = { array = ruling_party value = 4 }
									is_in_array = { array = ruling_party value = 19 }
								}
								OR = {
									has_civil_war = no
									AND = {
										has_civil_war = yes
										tag = [ROOTTAG]
									}
								}
							}
						}
					}
					ROOTTAG = "[?civil_war_original_tag.GetTag]"
				}
			}

			else = {
				meta_trigger = {
					text = {
						NOT = { has_cosmetic_tag = [ROOTTAG]_AUTH_S }
						OR = {
							is_in_array = { array = ruling_party value = 10 }
							is_in_array = { array = ruling_party value = 23 }
							AND = {
								OR = {
									is_in_array = { array = ruling_party value = 4 }
									is_in_array = { array = ruling_party value = 19 }
								}
								OR = {
									has_civil_war = no
									AND = {
										has_civil_war = yes
										tag = [ROOTTAG]
									}
								}
							}
						}
					}
					ROOTTAG = "[?original_tag.GetTag]"
				}
			}
			if = {
				limit = { tag = PER }
				NOT = {
					OR = {
						emerging_hardline_shiite_are_in_power = yes
						emerging_moderate_shiite_are_in_power = yes
					}
				}
			}
			if = {
				limit = { tag = AZE }
				NOT = {
					OR = {
						emerging_hardline_shiite_are_in_power = yes
						emerging_moderate_shiite_are_in_power = yes
					}
				}
			}
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision Special_flag"

			if = {
				limit = { has_country_flag = is_civil_war_faction }
				meta_effect = {
					text = { set_cosmetic_tag = [ROOTTAG]_AUTH_S }
					ROOTTAG = "[?civil_war_original_tag.GetTag]"
				}
			}
			else = {
				meta_effect = {
					text = { set_cosmetic_tag = [ROOTTAG]_AUTH_S }
					ROOTTAG = "[?original_tag.GetTag]"
				}
				custom_effect_tooltip = tooltip_change_flag
			}
		}
	}

	More_Special_flag = {
		icon = GFX_decision_generic_form_nation
		cost = 0
		ai_will_do = { factor = 1 }
		allowed = {
			NOT = {
				original_tag = BOS
				original_tag = SMA
				original_tag = RAJ
				original_tag = BUL
			}
		}
		visible = {
			NOT = {
				has_cosmetic_tag = CZR_union
				has_cosmetic_tag = GER_AUTH_S_communism
				has_cosmetic_tag = GER_com_reb_tech
				has_cosmetic_tag = EGY_COPTIC
				original_tag = BOS
			}
			has_country_flag = dynamic_flag
			has_elections = no
			if = {
				limit = { has_country_flag = is_civil_war_faction }
				if = {
					limit = {
						has_country_flag = dynamic_flag_edgy
						has_global_flag = GAME_RULE_allow_edgy_flags
						is_in_array = { array = ruling_party value = 21 }
					}
					meta_trigger = {
						text = { NOT = { has_cosmetic_tag = [ROOTTAG]_AUTH_SS_EDGY } }
						ROOTTAG = "[?civil_war_original_tag.GetTag]"
					}
				}
				else = {
					meta_trigger = {
						text = { NOT = { has_cosmetic_tag = [ROOTTAG]_AUTH_SS } }
						ROOTTAG = "[?civil_war_original_tag.GetTag]"
					}
				}
			}

			else = {
				if = {
					limit = {
						has_country_flag = dynamic_flag_edgy
						has_global_flag = GAME_RULE_allow_edgy_flags
						is_in_array = { array = ruling_party value = 21 }
					}
					meta_trigger = {
						text = { NOT = { has_cosmetic_tag = [ROOTTAG]_AUTH_SS_EDGY } }
						ROOTTAG = "[?original_tag.GetTag]"
					}
				}
				else = {
					meta_trigger = {
						text = { NOT = { has_cosmetic_tag = [ROOTTAG]_AUTH_SS } }
						ROOTTAG = "[?original_tag.GetTag]"
					}
				}
			}
			meta_trigger = {
				text = { NOT = { has_cosmetic_tag = [ROOTTAG]_AUTH_SS } }
				ROOTTAG = "[?original_tag.GetTag]"
			}
			OR = {
				is_in_array = { array = ruling_party value = 21 }
				is_in_array = { array = ruling_party value = 8 }
				is_in_array = { array = ruling_party value = 9 }
				is_in_array = { array = ruling_party value = 11 }
			}
			if = {
				limit = { tag = PER }
				NOT = {
					OR = {
						emerging_hardline_shiite_are_in_power = yes
						emerging_moderate_shiite_are_in_power = yes
					}
				}
			}
			if = {
				limit = { tag = AZE }
				NOT = {
					OR = {
						emerging_hardline_shiite_are_in_power = yes
						emerging_moderate_shiite_are_in_power = yes
					}
				}
			}
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision More_Special_flag"

			if = {
				limit = { has_country_flag = is_civil_war_faction }
				if = {
					limit = {
						has_country_flag = dynamic_flag_edgy
						has_global_flag = GAME_RULE_allow_edgy_flags
						is_in_array = { array = ruling_party value = 21 }
					}
					meta_effect = {
						text = { set_cosmetic_tag = [ROOTTAG]_AUTH_SS_EDGY }
						ROOTTAG = "[?civil_war_original_tag.GetTag]"
					}
				}
				else = {
					meta_effect = {
						text = { set_cosmetic_tag = [ROOTTAG]_AUTH_SS }
						ROOTTAG = "[?civil_war_original_tag.GetTag]"
					}
				}
			}
			else = {
				if = {
					limit = {
						has_country_flag = dynamic_flag_edgy
						has_global_flag = GAME_RULE_allow_edgy_flags
						is_in_array = { array = ruling_party value = 21 }
					}
					meta_effect = {
						text = { set_cosmetic_tag = [ROOTTAG]_AUTH_SS_EDGY }
						ROOTTAG = "[?original_tag.GetTag]"
					}
				}
				else = {
					meta_effect = {
						text = { set_cosmetic_tag = [ROOTTAG]_AUTH_SS }
						ROOTTAG = "[?original_tag.GetTag]"
					}
				}
				custom_effect_tooltip = tooltip_change_flag
			}
		}
	}

	Rebel_flag = {
		icon = GFX_decision_generic_form_nation
		cost = 0
		ai_will_do = { factor = 1 }
		allowed = {
			NOT = {
				original_tag = BOS
				original_tag = SMA
				original_tag = RAJ
				original_tag = BUL
			}
		}
		visible = {
			has_country_flag = dynamic_rebel_flag
			has_civil_war = yes

			if = {
				limit = { has_country_flag = is_civil_war_faction }
				meta_trigger = {
					text = {
						if = {
							limit = { NOT = { tag = original_tag } }
							any_country_with_original_tag = {
								original_tag_to_check = ROOT
								has_cosmetic_tag = [ROOTTAG]
							}
						}
						else = { always = no }
						NOT = {
							has_cosmetic_tag = [ROOTTAG]_REB
							tag = [ROOTTAG]
						}
					}
					ROOTTAG = "[?civil_war_original_tag.GetTag]"
				}
				NOT = {
					is_in_array = { array = ruling_party value = 4 }
					is_in_array = { array = ruling_party value = 19 }
					is_in_array = { array = ruling_party value = 22 }
					is_in_array = { array = ruling_party value = 21 }
					is_in_array = { array = ruling_party value = 8 }
					is_in_array = { array = ruling_party value = 9 }
					is_in_array = { array = ruling_party value = 10 }
					is_in_array = { array = ruling_party value = 11 }
					is_in_array = { array = ruling_party value = 23 }
					is_in_array = { array = ruling_party value = 0 }
					is_in_array = { array = ruling_party value = 7 }
					is_in_array = { array = ruling_party value = 13 }
				}
			}
			else = {
				meta_trigger = {
					text = {
						if = {
							limit = { NOT = { tag = original_tag } }
							any_country_with_original_tag = {
								original_tag_to_check = ROOT
								has_cosmetic_tag = [ROOTTAG]
							}
						}
						else = { always = no }
						NOT = {
							has_cosmetic_tag = [ROOTTAG]_REB
							tag = [ROOTTAG]
						}
					}
					ROOTTAG = "[?original_tag.GetTag]"
				}
				NOT = {
					is_in_array = { array = ruling_party value = 4 }
					is_in_array = { array = ruling_party value = 19 }
					is_in_array = { array = ruling_party value = 22 }
					is_in_array = { array = ruling_party value = 21 }
					is_in_array = { array = ruling_party value = 8 }
					is_in_array = { array = ruling_party value = 9 }
					is_in_array = { array = ruling_party value = 10 }
					is_in_array = { array = ruling_party value = 11 }
					is_in_array = { array = ruling_party value = 23 }
					is_in_array = { array = ruling_party value = 0 }
					is_in_array = { array = ruling_party value = 7 }
					is_in_array = { array = ruling_party value = 13 }
				}
			}
			if = {
				limit = { tag = PER }
				NOT = {
					OR = {
						emerging_hardline_shiite_are_in_power = yes
						emerging_moderate_shiite_are_in_power = yes
					}
				}
			}
			if = {
				limit = { tag = AZE }
				NOT = {
					OR = {
						emerging_hardline_shiite_are_in_power = yes
						emerging_moderate_shiite_are_in_power = yes
					}
				}
			}
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision Rebel_flag"

			if = {
				limit = { has_country_flag = is_civil_war_faction }
				meta_effect = {
					text = { set_cosmetic_tag = [ROOTTAG]_REB }
					ROOTTAG = "[?civil_war_original_tag.GetTag]"
				}
			}
			else = {
				meta_effect = {
					text = { set_cosmetic_tag = [ROOTTAG]_REB }
					ROOTTAG = "[?original_tag.GetTag]"
				}
				custom_effect_tooltip = tooltip_change_flag
			}
		}
	}

	Special_Rebel_flag = {
		icon = GFX_decision_generic_form_nation
		cost = 0
		ai_will_do = { factor = 1 }
		allowed = {
			NOT = {
				original_tag = RAJ
				original_tag = BOS
				original_tag = SMA
				original_tag = BUL
			}
		}
		visible = {
			has_country_flag = dynamic_rebel_flag
			has_civil_war = yes
			if = {
				limit = { has_country_flag = is_civil_war_faction }
				meta_trigger = {
					text = {
						NOT = { has_cosmetic_tag = [ROOTTAG]_REB_S }
						OR = {
							is_in_array = { array = ruling_party value = 22 }
							AND = {
								OR = {
									is_in_array = { array = ruling_party value = 4 }
									is_in_array = { array = ruling_party value = 19 }
								}
								NOT = { tag = [ROOTTAG] }
							}
						}
					}
					ROOTTAG = "[?civil_war_original_tag.GetTag]"
				}
			}
			else = {
				meta_trigger = {
					text = {
						NOT = { has_cosmetic_tag = [ROOTTAG]_REB_S }
						OR = {
							is_in_array = { array = ruling_party value = 22 }
							AND = {
								OR = {
									is_in_array = { array = ruling_party value = 4 }
									is_in_array = { array = ruling_party value = 19 }
								}
								NOT = { tag = [ROOTTAG] }
							}
						}
					}
					ROOTTAG = "[?original_tag.GetTag]"
				}
			}
			if = {
				limit = { tag = PER }
				NOT = {
					OR = {
						emerging_hardline_shiite_are_in_power = yes
						emerging_moderate_shiite_are_in_power = yes
					}
				}
			}
			if = {
				limit = { tag = AZE }
				NOT = {
					OR = {
						emerging_hardline_shiite_are_in_power = yes
						emerging_moderate_shiite_are_in_power = yes
					}
				}
			}
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision Special_Rebel_flag"

			if = {
				limit = { has_country_flag = is_civil_war_faction }
				meta_effect = {
					text = { set_cosmetic_tag = [ROOTTAG]_REB_S }
					ROOTTAG = "[?civil_war_original_tag.GetTag]"
				}
			}
			else = {
				meta_effect = {
					text = { set_cosmetic_tag = [ROOTTAG]_REB_S }
					ROOTTAG = "[?original_tag.GetTag]"
				}
				custom_effect_tooltip = tooltip_change_flag
			}
		}
	}

	add_dynamic_flag = {
		cost = 0
		ai_will_do = { factor = 1 }
		visible = {
			NOT = {
				has_country_flag = dynamic_flag
				has_country_flag = fix_your_broken_dynamic_tags
				has_cosmetic_tag = CZR_union
				has_cosmetic_tag = GER_AUTH_S_communism
				has_cosmetic_tag = GER_com_reb_tech
				has_cosmetic_tag = EGY_COPTIC
			}
			NOT = {
				has_cosmetic_tag = PER_mullahs_government
			}
			if = {
				limit = { NOT = { tag = original_tag } }
				any_country_with_original_tag = {
					original_tag_to_check = ROOT
					has_country_flag = dynamic_flag
				}
			}
			else = { always = no }
			if = {
				limit = { tag = PER }
				NOT = {
					OR = {
						emerging_hardline_shiite_are_in_power = yes
						emerging_moderate_shiite_are_in_power = yes
					}
				}
			}
			if = {
				limit = { tag = AZE }
				NOT = {
					OR = {
						emerging_hardline_shiite_are_in_power = yes
						emerging_moderate_shiite_are_in_power = yes
					}
				}
			}
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision add_dynamic_flag"
			set_country_flag = dynamic_flag
			set_country_flag = dynamic_rebel_flag
			if = {
				limit = {
					any_country_with_original_tag = {
						original_tag_to_check = ROOT
						has_country_flag = is_civil_war_faction
					}
				}
				set_country_flag = is_civil_war_faction
				random_country_with_original_tag = {
					original_tag_to_check = ROOT
					set_variable = { civil_war_original_tag@ROOT = civil_war_original_tag@THIS }
				}
			}
		}
	}

	#Custom
	SER_flag_change = {
		icon = GFX_decision_generic_form_nation
		cost = 0
		ai_will_do = { factor = 1 }
		allowed = { original_tag = SER }
		visible = {
			NOT = {
				has_country_flag = dynamic_flag
				owns_state = 134
			}
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision SER_flag_change"
			set_cosmetic_tag = SER
			set_cosmetic_tag = dynamic_flag
			set_cosmetic_tag = dynamic_rebel_flag
			custom_effect_tooltip = tooltip_change_flag
		}
	}

	USA_Reset_flag = {
		icon = GFX_decision_generic_form_nation
		cost = 0
		ai_will_do = { factor = 1 }
		allowed = { original_tag = USA }
		visible = {
			has_country_flag = dynamic_flag
			has_elections = yes

			OR = {
				AND = {
					OR = {
						has_cosmetic_tag = USA
						has_cosmetic_tag = USA_51
					}
					has_country_flag = flag_update_required
				}
				NOT = {
					has_cosmetic_tag = USA
					has_cosmetic_tag = USA_51
					has_cosmetic_tag = USA_52

					AND = {
						has_civil_war = yes
						OR = {
							NOT = { tag = USA }
							has_cosmetic_tag = USA_REB
							has_cosmetic_tag = USA_REB_S
						}
					}
				}
			}
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision USA_Reset_flag"
			if = {
				limit = { check_variable = { USA_state_number = 50 } }
				set_cosmetic_tag = USA
			}
			else_if = {
				limit = { check_variable = { USA_state_number = 51 } }
				set_cosmetic_tag = USA_51
			}
			else = {
				set_cosmetic_tag = USA_52
			}
			clr_country_flag = flag_update_required
		}
	}

	#Historic Flag Changes

	#New flag
	MAU_NEW_flag = {
		icon = GFX_decision_generic_form_nation
		cost = 0
		ai_will_do = { factor = 1 }
		allowed = { tag = MAU }
		visible = {
			has_cosmetic_tag = MAU
			date > 2017.11.28
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision MAU_NEW_flag"
			set_country_flag = dynamic_flag_new
			set_cosmetic_tag = MAU_NEW
		}
	}

	IRQ_NEW_flag = {
		icon = GFX_decision_generic_form_nation
		cost = 0
		ai_will_do = { factor = 1 }
		allowed = { tag = IRQ }
		visible = {
			NOT = { has_cosmetic_tag = IRQ_NEW }
			date > 2008.01.22
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision IRQ_NEW_flag"
			set_country_flag = dynamic_flag_new
			set_cosmetic_tag = IRQ_NEW
		}
	}

	DRC_NEW_flag = {
		icon = GFX_decision_generic_form_nation
		cost = 0
		ai_will_do = { factor = 1 }
		allowed = { tag = DRC }
		visible = {
			has_cosmetic_tag = DRC_OLD
			NOT = {
				has_war_with = MLC
				has_war_with = RCD
			}
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision DRC_NEW_flag"
			set_country_flag = dynamic_flag
			set_country_flag = dynamic_rebel_flag

			set_cosmetic_tag = DRC_AUTH
		}
	}

	RWA_NEW_flag = {
		icon = GFX_decision_generic_form_nation
		cost = 0
		ai_will_do = { factor = 1 }
		allowed = { tag = DRC }
		visible = {
			has_cosmetic_tag = RWA_OLD
			date > 2001.10.25
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision RWA_NEW_flag"
			set_country_flag = dynamic_flag

			set_cosmetic_tag = RWA
		}
	}

	### Shitty memes ###
	ISI_SHEEP_flag = {
		icon = GFX_decision_generic_form_nation
		cost = 0
		ai_will_do = { factor = 0 }
		allowed = { original_tag = ISI }
		visible = {
			original_tag = ISI
			NOT = { has_cosmetic_tag = ISI_SHEEP }
			controls_state = 16
			controls_state = 750
			controls_state = 751
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision ISI_SHEEP_flag"
			set_cosmetic_tag = ISI_SHEEP
			clr_country_flag = dynamic_flag
			custom_effect_tooltip = tooltip_change_flag
		}
	}

	GEO_USA_flag = {
		icon = GFX_decision_generic_form_nation
		cost = 0
		ai_will_do = { factor = 0 }
		allowed = { original_tag = GEO }
		visible = {
			original_tag = GEO
			NOT = { has_cosmetic_tag = GEO_USA }
			owns_state = 792
			NOT = {
				owns_state = 705
				owns_state = 706
				owns_state = 707
				owns_state = 708
				owns_state = 466
			}
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision GEO_USA_flag"
			set_cosmetic_tag = GEO_USA
			clr_country_flag = dynamic_flag
			add_state_core = 792
			set_capital = { state = 792 }
			remove_state_core = 705
			remove_state_core = 706
			remove_state_core = 707
			remove_state_core = 708
			remove_state_core = 466
			custom_effect_tooltip = tooltip_change_flag
		}
	}
}




#I hate myself.
#Me too :>

#Bruh