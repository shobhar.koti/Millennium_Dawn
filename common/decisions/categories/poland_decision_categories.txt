POL_influence_neighbours_category = {
	allowed = {
		original_tag = POL
	}

	visible = {
		has_country_flag = POL_nationalist_influence
	}
}

POL_military_intervention_category = {
	allowed = {
		original_tag = POL
	}

	visible = {
		has_country_flag = POL_enabled_justification
	}
}

POL_state_controlled_economy_category = {
	allowed = {
		original_tag = POL
	}

	visible = {
		has_completed_focus = POL_prl_was_a_failure
		#emerging_communist_state_are_in_power = yes
	}
}

POL_revert_others_changes_category = {
	allowed = {
		original_tag = POL
	}

	visible = {
		# for now only for existing parties
		OR = {
			emerging_communist_state_are_in_power = yes
			emerging_anarchist_communism_are_in_power = yes
			nationalist_fascist_are_in_power = yes
			nationalist_right_wing_populists_are_in_power = yes
			western_conservatism_are_in_power = yes
			western_liberals_are_in_power = yes
		}
	}
}

POL_visegrad_unification2_category = {
	allowed = {
		original_tag = POL
	}
	icon = GFX_decision_category_v4_cat
	picture = GFX_decision_category_picture_v4

	visible = {
		has_completed_focus = POL_discussion_of_the_world_situation
		CZE = {
			is_subject_of = POL
		}
		NOT = {
			SLO = {
				is_subject_of = POL
			}
		}
	}
}

POL_visegrad_unification3_category = {
	allowed = {
		original_tag = POL
	}
	icon = GFX_decision_category_v4_cat
	picture = GFX_decision_category_picture_v4

	visible = {
		has_completed_focus = POL_mission_hungary
		CZE = {
			is_subject_of = POL
		}
		SLO = {
			is_subject_of = POL
		}
		NOT = {
			HUN = {
				is_subject_of = POL
			}
		}
	}
}

POL_visegrad_unification4_category = {
	allowed = {
		original_tag = POL
	}
	icon = GFX_decision_category_v4_cat
	picture = GFX_decision_category_picture_v4

	visible = {
		OR = {
			has_cosmetic_tag = POL_QUAD
			has_cosmetic_tag = POL_LIT_QUAD
		}
		NOT = {
			has_completed_focus = POL_permament_friendship
		}
	}
}

POL_territorial_defense_forces_category = {
	allowed = {
		original_tag = POL
	}
	icon = GFX_decision_category_generic_arms_trade

	visible = {
		has_completed_focus = POL_creation_of_the_territorial_defense_forces
		NOT = {
			has_war = yes
		}
	}
}

POL_k2pl_export = {
	allowed = {
		original_tag = POL
	}
	icon = GFX_decision_category_generic_arms_trade

	visible = {
		has_completed_focus = POL_production_and_export_of_the_k2pl_tank
	}
}

POL_air_defense_category = {
	allowed = {
		original_tag = POL
	}
	icon = GFX_decision_category_generic_arms_trade

	visible = {
		has_completed_focus = POL_air_defense_troops
	}
}

POL_intensified_army_training_category = {
	allowed = {
		original_tag = POL
	}
	icon = GFX_decision_category_generic_arms_trade

	visible = {
		has_completed_focus = POL_intensified_army_training
	}
}

POL_monuments_removal_category = {
	allowed = {
		original_tag = POL
	}

	visible = {
		has_completed_focus = POL_removal_of_communist_monuments
	}
}

POL_korwin_walesa_bop_category = {
	allowed = {
		original_tag = POL
	}
	icon = GFX_decision_category_power_struggle

	priority = 90

	visible = {
		has_completed_focus = POL_korwin_is_in_power
		NOT = {
			has_country_flag = POL_korwin_wins
		}
	}
}

POL_korwin_win_bop_category = {
	allowed = {
		original_tag = POL
	}
	icon = GFX_decision_category_power_struggle

	priority = 90

	visible = {
		has_country_flag = POL_korwin_wins
	}
}

POL_walesa_korwin_bop_category = {
	allowed = {
		original_tag = POL
	}
	icon = GFX_decision_category_power_struggle

	priority = 90

	visible = {
		has_completed_focus = POL_walesa_is_in_power
	}
}

POL_polish_ukrainian_war_category = {

	icon = GFX_decision_category_border_conflicts

	allowed = {
		original_tag = POL
	}

	priority = 100

	visible = {
		has_war_with = UKR
		original_tag = POL
	}
}

POL_ukraine_cooperation_category = {
	allowed = {
		original_tag = POL
	}
	icon = GFX_decision_category_generic_foreign_policy

	visible = {
		UKR = {
			has_country_flag = POL_ukraine_cooperation
		}
	}
}

POL_ukraine_in_nato = {
	allowed = {
		original_tag = POL
	}
	icon = GFX_decision_category_generic_political_actions

	visible = {
		has_country_flag = POL_ukraine_nato
		UKR = {
			NOT = {
				OR = {
					has_idea = NATO_member
					has_country_flag = POL_ukraine_rej
				}
			}
		}
	}
}

POL_SOV_ukraine_in_nato = {
	allowed = {
		original_tag = SOV
	}
	icon = GFX_decision_category_generic_political_actions

	visible = {
		POL = {
			has_country_flag = POL_ukraine_nato
			UKR = {
				NOT = {
					OR = {
						has_idea = NATO_member
						has_country_flag = POL_ukraine_rej
					}
				}
			}
		}
	}
}

POL_kaliningrad_problem = {
	allowed = {
		original_tag = POL
	}
	icon = GFX_decision_category_generic

	visible = {
		has_completed_focus = POL_make_two_nations_one_again
		SOV = {
			OR = {
				controls_state = 642
				has_war_with = POL
			}
		}
	}
}

POL_polsa_construction_category = {
	allowed = {
		original_tag = POL
	}
	icon = GFX_decision_category_generic_industry
	picture = GFX_decision_urban_development_fund_category

	visible = {
		has_completed_focus = POL_begin_contruction_of_space_company
	}
}

POL_polsa_research_facility_category = {
	allowed = {
		original_tag = POL
	}
	icon = GFX_decision_reactor_dec

	visible = {
		has_country_flag = POL_polsa_6
	}
}

POL_moon_launch_mission = {
	allowed = {
		original_tag = POL
	}
	icon = GFX_decision_asteroid_mining

	visible = {
		has_completed_focus = POL_moon_mission_preparations
	}
}

POL_cua_category = {
	scripted_gui = POL_communism_uprising_again_scripted_gui
	visible_when_empty = yes
	icon = GFX_decision_kavkaz_revol
	priority = 200

	allowed = {
		original_tag = POL
	}

	visible = {
		has_country_flag = POL_communism_uprising_again_activation
		NOT = {
			OR = {
				has_idea = POL_solidarity_strength5_idea
				has_idea = POL_communism_domination5_idea
			}
			is_ai = yes
		}
	}
}

POL_control_everything_category = {
	visible_when_empty = no
	icon = GFX_decision_category_coms_cat
	priority = 50

	allowed = {
		original_tag = POL
	}

	visible = {
		has_completed_focus = POL_control_everything
	}
}

POL_department_of_foreign_influence_category = {
	scripted_gui = POL_dofi_scripted_gui
	visible_when_empty = no
	icon = GFX_decision_category_sov_great_patriotic_war
	priority = 200

	allowed = {
		original_tag = POL
	}

	visible = {
		has_completed_focus = POL_department_of_foreign_influence
	}
}

POL_six_year_plan_category = {
	visible_when_empty = no
	icon = GFX_decision_category_generic_industry
	priority = 200

	allowed = {
		original_tag = POL
	}

	visible = {
		has_country_flag = POL_six_year_plan
	}
}

POL_communists_vs_social_democrats_category = {
	visible_when_empty = no
	icon = GFX_decision_hol_draw_up_staff_plans
	priority = 500

	allowed = {
		original_tag = POL
	}

	visible = {
		has_completed_focus = POL_betray_the_communists
		has_civil_war = yes
		has_government = communism
	}
}

POL_civ_mil_category = {
	visible_when_empty = no
	icon = GFX_decision_category_generic_prospect_for_resources
	priority = 500

	allowed = {
		original_tag = POL
	}

	visible = {
		has_completed_focus = POL_improve_civilian_and_military_industry
		has_government = communism
	}
}