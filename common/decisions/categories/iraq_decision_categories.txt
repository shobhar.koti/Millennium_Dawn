IRQ_MOBILIZE = {
	icon = political_actions

	allowed = {
		original_tag = IRQ
	}

	visible = {
		original_tag = IRQ
	}
}

IRQ_preparation_for_the_war = {
	icon = political_actions

	allowed = {
		original_tag = IRQ
	}
	priority = 200
	visible = {
		original_tag = IRQ
		has_country_flag = IRQ_prepare_for_usa_war
	}
}
IRQ_economic_union_menu = {
	icon = political_actions

	allowed = {
		original_tag = IRQ
	}
	priority = 50
	visible = {
		original_tag = IRQ
		has_country_flag = economic_union_formed_iraq
	}
}
IRQ_iraq_war = {
	icon = political_actions

	allowed = {
		original_tag = IRQ
	}
	priority = 50
	visible_when_empty = yes
	visible = {
		original_tag = IRQ
		has_country_flag = IRQ_iraq_war_ongoing
	}
}
IRQ_the_constitution = {
	icon = political_actions

	allowed = {
		original_tag = IRQ
	}
	priority = 250
	visible = {
		original_tag = IRQ
		has_country_flag = IRQ_iraqi_constitution_draft
	}
}
IRQ_civil_war_mechanic = {
	icon = GFX_decision_generic_scorched_earth
	scripted_gui = Iraq_civil_war_gui
	allowed = {
		original_tag = IRQ
	}
	priority = 450
	visible_when_empty = yes
	visible = {
		original_tag = IRQ
		has_country_flag = IRQ_insurgency_active
	}
}
IRQ_political_decisions = {
	icon = political_actions
	allowed = {
		original_tag = IRQ
	}
	priority = 500
	#visible_when_empty = yes
	visible = {
		original_tag = IRQ
	}
}
IRQ_regional_baathism_decisions = {
	icon = political_actions
	allowed = {
		original_tag = IRQ
	}
	priority = 500
	#visible_when_empty = yes
	visible = {
		original_tag = IRQ
	}
}
IRQ_black_gold_decisions = {
	icon = GFX_decision_category_generic_prospect_for_resources
	allowed = {
		original_tag = IRQ
	}
	priority = 500
	#visible_when_empty = yes
	visible = {
		original_tag = IRQ
		has_country_flag = IRQ_aramco_time
		country_exists = SAU
	}
}
IRQ_clergy_balance_of_power_category = {
	icon = generic_independence

	priority = 10

	allowed = {
		original_tag = IRQ
	}
}
IRQ_debaath_balance_of_power_category = {
	icon = generic_independence

	priority = 10

	allowed = {
		original_tag = IRQ
	}
}

IRQ_sunni_in_politics = {
	icon = generic_independence
	visible = {
		has_country_flag = IRQ_sunni_mechanic
	}
	priority = 300
	visible_when_empty = yes
}

IRQ_fao_decisions = {
	icon = generic_independence
	visible = {
		has_country_flag = IRQ_fao_flag
	}
	priority = 300
	visible_when_empty = yes
}

IRQ_pmf_decisions = {
	icon = generic_independence
	visible = {
		has_country_flag = IRQ_pmf_system
	}
	scripted_gui = IRQ_pmf_recovery
	priority = 300
	visible_when_empty = yes
}

IRQ_spring_of_islam = {
	icon = generic_independence
	visible = {
		has_country_flag = IRQ_spring_of_islam
	}
	priority = 300
}

IRQ_kurdish_friends = {
	icon = generic_independence
	visible = {
		has_country_flag = IRQ_kurd_s
		neutrality_neutral_communism_are_in_power = yes
	}
	priority = 300
}

IRQ_saddams_pissfits = {
	icon = generic_independence
	visible = {
		OR = {
			has_country_leader = { name = "Saddam Hussein" ruling_only = yes }
			has_country_leader = { name = "Uday Hussein" ruling_only = yes }
		}
	}
	priority = 700
	visible_when_empty = yes
}