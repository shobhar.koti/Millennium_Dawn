# Author(s): AngriestBird, Sheep, Fire_Hair, Borgers
DEN_decision_danish_revolutionary_thought = {
	priority = 200
	icon = generic
	allowed = { original_tag = DEN }
	visible = {
		has_completed_focus = DEN_broken_system
		emerging_communist_state_are_in_power = yes
	}
}

DEN_decision_Investments_Global = {
	priority = 200
	icon = generic
	allowed = { original_tag = DEN }
	visible = {
		has_completed_focus = DEN_Logistic_Global_Investments
	}
}

DEN_decision_Investments_Global_II = {
	priority = 200
	icon = generic
	allowed = { original_tag = DEN }
	visible = {
		has_completed_focus = DEN_Logistic_Global_Investments
	}
}

DEN_Danish_Defence_Force = {
	priority = 200
	icon = military_operation
	allowed = { original_tag = DEN }
}

DEN_urban_development_fund_category = {
	priority = 200
	icon = generic_prospect_for_resources
	allowed = { original_tag = DEN }
	visible = { has_completed_focus = DEN_urban_improvement_fund }
}

DEN_visitdenmark_category = {
	priority = 200
	icon = generic
	allowed = { original_tag = DEN }
	visible = { has_idea = DEN_idea_Visitdenmark }
}

DEN_colonial_separatism_category = {
	priority = 200
	icon = generic
	allowed = { original_tag = DEN }
	visible = { has_completed_focus = DEN_status_of_the_colonies }
}

DEN_generic_danish_political_decisions_category = {
	priority = 180
	icon = generic
	allowed = { original_tag = DEN }
}

decision_danish_revolutionary_thought_category = {
	priority = 180
	icon = generic
	allowed = { original_tag = DEN }
}

DEN_euro_question_decisions_category = {
	priority = 180
	icon = generic
	allowed = { original_tag = DEN }
}

DEN_decision_Agriculture = {
	priority = 200
	icon = generic
	allowed = { original_tag = DEN }
	visible = { has_completed_focus = DEN_Agriculture }
}
