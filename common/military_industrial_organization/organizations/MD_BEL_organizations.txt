BEL_fn_herstal_materiel_manufacturer = {
	allowed = { original_tag = BEL }
	icon = GFX_idea_FN_Herstal_BEL
	name = BEL_fn_herstal_materiel_manufacturer
	include = generic_infantry_equipment_organization
}

BEL_cmi_defence_tank_manufacturer = {
	allowed = { original_tag = BEL }
	icon = GFX_idea_CMI_Defence_BEL
	name = BEL_cmi_defence_tank_manufacturer
	include = generic_tank_equipment_organization
}

BEL_sabiex_international_tank_manufacturer = {
	allowed = { original_tag = BEL }
	icon = GFX_idea_Sabiex_International_BEL
	name = BEL_sabiex_international_tank_manufacturer
	include = generic_specialized_tank_organization
}

BEL_sabca_aircraft_manufacturer = {
	allowed = { original_tag = BEL }
	icon = GFX_idea_ASABCA_BEL
	name = BEL_sabca_aircraft_manufacturer
	include = generic_air_equipment_organization
}
