BLR_BELSPETSVNESHTECHNIKA_materiel_manufacturer = {
	allowed = { original_tag = BLR }
	icon = GFX_idea_MZKT_BLR
	name = BLR_falck_schmidt_tank_manufacturer
	include = generic_infantry_equipment_organization
}

BLR_MZKT_tank_manufacturer = {
	allowed = { original_tag = BLR }
	icon = GFX_idea_BLR_BELSPETSVNESHTECHNIKA
	name = BLR_MZKT_tank_manufacturer
	include = generic_tank_equipment_organization
}

BLR_558_aircraft_repair_plant_aircraft_manufacturer = {
	allowed = { original_tag = BLR }
	icon = GFX_idea_BLR_558_aircraft_repair_plant
	name = BLR_558_aircraft_repair_plant_aircraft_manufacturer
	include = generic_air_equipment_organization
}
