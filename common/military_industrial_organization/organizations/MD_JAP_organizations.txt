JAP_howa_machinery_materiel_manufacturer = {
	allowed = { original_tag = JAP }
	icon = GFX_idea_Howa
	name = JAP_howa_machinery_materiel_manufacturer
	include = generic_infantry_equipment_organization
}

JAP_komatsu_tank_manufacturer = {
	allowed = { original_tag = JAP }
	icon = GFX_idea_Komatsu
	name = JAP_komatsu_tank_manufacturer
	include = generic_specialized_tank_organization
}

JAP_mitsubishi_heavy_industries_tank_manufacturer = {
	allowed = { original_tag = JAP }
	icon = GFX_idea_Mitsubishi_Heavy_Industries
	name = JAP_mitsubishi_heavy_industries_tank_manufacturer
	include = generic_tank_equipment_organization
}

JAP_mitsubishi_aerospace_tank_manufacturer = {
	allowed = { original_tag = JAP }
	icon = GFX_idea_Mitsubishi_aerospace
	name = JAP_mitsubishi_aerospace_tank_manufacturer
	include = generic_specialized_helicopter_organization
}

JAP_kawasaki_aerospace_company_tank_manufacturer = {
	allowed = { original_tag = JAP }
	icon = GFX_idea_kawasaki_aerospace
	name = JAP_kawasaki_aerospace_company_tank_manufacturer
	include = generic_specialized_helicopter_organization
}

JAP_mitsubishi_aerospace_aircraft_manufacturer = {
	allowed = { original_tag = JAP }
	icon = GFX_idea_Mitsubishi_aerospace
	name = JAP_mitsubishi_aerospace_aircraft_manufacturer
	include = generic_air_equipment_organization
}

JAP_kawasaki_aerospace_company_aircraft_manufacturer = {
	allowed = { original_tag = JAP }
	icon = GFX_idea_kawasaki_aerospace
	name = JAP_kawasaki_aerospace_company_aircraft_manufacturer
	# TODO: Replace with a heavy air MIO
	include = generic_air_equipment_organization
}

JAP_mitsubishi_shipbuilding_naval_manufacturer2 = {
	allowed = { original_tag = JAP }
	icon = GFX_idea_Mitsubishi_Heavy_Industries
	name = JAP_mitsubishi_shipbuilding_naval_manufacturer2
	include = generic_naval_light_equipment_organization
}

JAP_kawasaki_shipbuilding_naval_manufacturer2 = {
	allowed = { original_tag = JAP }
	icon = GFX_idea_Kawasaki_Shipbuilding
	name = JAP_kawasaki_shipbuilding_naval_manufacturer2
	include = generic_naval_equipment_organization
}

JAP_ihi_corporation_naval_manufacturer = {
	allowed = { original_tag = JAP }
	icon = GFX_idea_IHI
	name = JAP_ihi_corporation_naval_manufacturer
	include = generic_naval_equipment_organization
}

JAP_mitsubishi_shipbuilding_naval_manufacturer = {
	allowed = { original_tag = JAP }
	icon = GFX_idea_Mitsubishi_Heavy_Industries
	name = JAP_mitsubishi_shipbuilding_naval_manufacturer
	include = generic_sub_equipment_organization
}

JAP_kawasaki_shipbuilding_naval_manufacturer = {
	allowed = { original_tag = JAP }
	icon = GFX_idea_Kawasaki_Shipbuilding
	name = JAP_kawasaki_shipbuilding_naval_manufacturer
	include = generic_sub_equipment_organization
}