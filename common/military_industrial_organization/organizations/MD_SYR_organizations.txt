SYR_hmisho_vehicles = {
	allowed = { original_tag = SYR }
	icon = GFX_idea_Hmisho_Vehicles
	name = SYR_hmisho_vehicles
	include = generic_tank_equipment_organization
}

SYR_damascus_steel = {
	allowed = { original_tag = SYR }
	icon = GFX_idea_Damascus_Steel
	name = SYR_damascus_steel
	include = generic_infantry_equipment_organization
}