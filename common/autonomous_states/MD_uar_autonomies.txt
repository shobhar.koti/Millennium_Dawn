#Autonomy levels for United Arab Republic
autonomy_state = {
	id = autonomy_uar_state

	default = no
	is_puppet = no
	use_overlord_color = yes
	min_freedom_level = 0.60
	manpower_influence = 0

	rule = {
		can_access_market = yes
		can_be_spymaster = yes
		can_decline_call_to_war = yes
		can_not_declare_war = no
		units_deployed_to_overlord = no
	}

	modifier = {
		cic_to_overlord_factor = 0.0
		mic_to_overlord_factor = 0.0
		can_master_build_for_us = 1
		autonomy_manpower_share = 1.00
		extra_trade_to_overlord_factor = 0.2
		overlord_trade_cost_factor = -0.2
	}

	ai_subject_wants_higher = {
		factor = 1.0
	}

	ai_overlord_wants_lower = {
		factor = 1.0
	}

	ai_overlord_wants_garrison = {
		always = no
	}

	allowed = {
		is_arabic_nation = yes
		OVERLORD = {
			OR = {
				has_country_flag = formed_baathist_uar
				has_country_flag = united_baathist_uar
				has_country_flag = formed_neo_baathist_uar
				has_country_flag = united_neo_baathist_uar
			}
		}
	}

	can_take_level = {
	}

	can_lose_level = {
	}

}

autonomy_state = {
	id = autonomy_uar_province

	default = yes
	is_puppet = yes
	use_overlord_color = yes
	min_freedom_level = 0.20
	manpower_influence = 0.3

	rule = {
		can_access_market = yes
		can_be_spymaster = no
		can_decline_call_to_war = yes
		can_not_declare_war = yes
		units_deployed_to_overlord = no
	}

	modifier = {
		cic_to_overlord_factor = 0.25
		mic_to_overlord_factor = 0.25
		can_master_build_for_us = 1
		autonomy_manpower_share = 1.00
		extra_trade_to_overlord_factor = 0.4
		overlord_trade_cost_factor = -0.4
	}

	ai_subject_wants_higher = {
		factor = 1.0
	}

	ai_overlord_wants_lower = {
		factor = 1.0
	}

	ai_overlord_wants_garrison = {
		always = no
	}

	allowed = {
		is_arabic_nation = yes
		OVERLORD = {
			OR = {
				has_country_flag = formed_baathist_uar
				has_country_flag = united_baathist_uar
				has_country_flag = formed_neo_baathist_uar
				has_country_flag = united_neo_baathist_uar
			}
		}
	}

	can_take_level = {
		#Can't increase autonomy if dropped to regional command
		NOT = { has_autonomy_state = autonomy_uar_regional_command }
	}

	can_lose_level = {
	}

}

autonomy_state = {
	id = autonomy_uar_regional_command

	default = no
	is_puppet = yes
	use_overlord_color = yes
	min_freedom_level = 0
	manpower_influence = 0.7

	rule = {
		can_access_market = yes
		can_be_spymaster = no
		can_decline_call_to_war = yes
		can_not_declare_war = yes
		units_deployed_to_overlord = no
	}

	modifier = {
		cic_to_overlord_factor = 0.75
		mic_to_overlord_factor = 0.75
		can_master_build_for_us = 1
		autonomy_manpower_share = 1.00
		extra_trade_to_overlord_factor = 0.75
		overlord_trade_cost_factor = -0.75
	}

	ai_subject_wants_higher = {
		factor = 1.0
	}

	ai_overlord_wants_lower = {
		factor = 1.0
	}

	ai_overlord_wants_garrison = {
		always = no
	}

	allowed = {
		is_arabic_nation = yes
		OVERLORD = {
			OR = {
				has_country_flag = formed_baathist_uar
				has_country_flag = united_baathist_uar
				has_country_flag = formed_neo_baathist_uar
				has_country_flag = united_neo_baathist_uar
			}
		}
	}

	can_take_level = {
	}

	can_lose_level = {
	}

}