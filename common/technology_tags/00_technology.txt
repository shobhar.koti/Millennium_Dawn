# Do not change tags in here without changing every other reference to them.
# If adding new technology, make sure they are uniquely named.

technology_Categories = {

#######################################################MD4 EQUIPMENT###########################################

	### Armor Tab ###
	CAT_armor  #All Armor

		CAT_tanks #All tanks
			CAT_mbt #new - Main Battle Tank
			CAT_eng_mbt #new -Engi Tank
			CAT_rec_tank #new - Recon Tank

		CAT_afv #All Armored Fighting Vehicles
			CAT_ifv #new - Infantry Fighting Vehicle
			CAT_apc #new - Armoured peronel Carrier
			CAT_util #Utility Vehicle

		CAT_computer_systems 	#Computer Systems
		CAT_armour				#Armour
		CAT_armor_engines		#Engines
		CAT_armor_weapons		#Weapons
		CAT_afv_weapons			#Light AFV Weapons


	#### Arty Tab ###
	CAT_artillery #All Artillery/AA/AT

		CAT_arty #Towed Artillery
		CAT_sp_arty #Self Propelled Artillery
		CAT_sp_r_arty #Self Propelled Rocket Artillery

		CAT_aa #All Anti-Air
			CAT_l_aa # Hand Held AA
			CAT_sp_aa #Self propelled AA

		CAT_at #All AT
			CAT_l_at #Light AT
			CAT_h_at #Heavy AT

	### Infantry tab ###
	CAT_inf #All Infantry

		CAT_inf_wep #Infantry Weapons
		CAT_l_drone #Land Drone Equipment
		CAT_cnc #Command and Control Equipment

		CAT_nvg #Night Vision Googles
		CAT_special_forces #Special Forces
			CAT_airmobile
			CAT_airborne
			CAT_marine
		CAT_support_weapons #Support Weapons

		CAT_l_aa # Hand Held AA (yes its in two catagories)
		CAT_l_at #Light AT (yes its in two catagories)

	### Air ###
	CAT_air_eqp #All Air Equipment

		CAT_naval_air 			#All naval air
		CAT_fixed_wing 			#All fighters/bombers tab
		CAT_air_wpn 			#All Aircraft weapons
		CAT_air_spc 			#All Special Modules
		CAT_awacs				#AWACS
		CAT_avionics			#Avionics
		CAT_drones				#Drones
		CAT_wings				#Wingforms
		CAT_air_stealth			#Stealth Techs
		CAT_air_naval_weapons	#Naval Weapons & MAD
		CAT_air_ground_weapons	#Air to Ground Weapons & TGPs
		CAT_air_to_air_weapons	#Air to Air Weapons
		CAT_air_countermeasures	#Countermeasures
		CAT_air_engine			#Engine
		CAT_air_camera			#Target Pods

		### Bombers ###
		CAT_h_air #Heavy Aircraft
			CAT_str_bomber #Strategic Bomber
			CAT_naval_plane #Maritime Patrol
			CAT_cas #Close Air Support
			CAT_trans_plane #Transport Plane
			CAT_awacs #AWACS
			CAT_large_plane

		### Fighter Tab ###
		CAT_fighter #All Fighters
			CAT_mr_fighter #Multi Role Fighters
			CAT_cv_mr_fighter #Carrier Multi Role Fighters
			CAT_s_fighter #Strike Fighters
			CAT_medium_plane
			CAT_small_plane

			CAT_l_fighter #Light Fighter/trainer
				CAT_l_s_fighter #Light Strike Fighter
				CAT_cv_l_s_fighter #Carrier Light Strike Fighter

			CAT_as_fighter #Air Supperiority Fighter
			CAT_a_uav #Unmanned Combat Aireial Vehicle

		### Helicopter Tab ###
		CAT_heli #All Helicopters
			CAT_trans_heli #Transport Helicopter
			CAT_atk_heli #Attack Helicopter





	### Naval ###
	CAT_naval_eqp #All Naval Ships

		CAT_naval_engine		#Engines
		CAT_naval_electronics	#Fire Control/ Electronic Warfare
		CAT_naval_armament		#Armament
		CAT_naval_cannons		#Ship-Based Cannons
		CAT_pds					#Point Defense Systems
		CAT_as_missiles			#Anti-Ship Missiles
		CAT_aa_missiles			#AA Missiles
		CAT_vls_systems			#VLS Systems
		CAT_vls_air_systems		#VLS Systems Air
		CAT_vls_land_systems		#VLS Systems Land
		CAT_torpedoes			#Torpedoes
		CAT_landing_craft		#Landing Craft

	CAT_naval_misc #All Naval Misc Techs

		#Submarines
		CAT_sub #All Submarines

			CAT_nuke_sub #Nuclear Submarines
				CAT_atk_sub #Nuclear Attack Submarine
				CAT_m_sub #Nuclear Missile Submarine

			CAT_d_sub #Disesel Submarine

		#Surface Ships
		CAT_surface_ship #All Surface Ships
			CAT_n_cruiser #Nuclear Missile Cruiser
			CAT_m_cruiser #Missile Cruiser
			CAT_destroyer #Missile Destroyer
			CAT_frigate #Frigate
			CAT_corvette #Corvette

		#Carrier
		CAT_carrier #All Carriers
			CAT_n_cv #nuclear carrier
			CAT_cv #Aircraft Carrier
			CAT_lha #Landing Helicopter Assault (LHA)
			CAT_lpd #Amphibious transport dock/landing platform/dock (LPD)

	### Civilian ###
		CAT_ai
		CAT_nfibers
		CAT_3d
		CAT_genes
		CAT_nuclear
		CAT_nuclear_reactors
		CAT_naval_reactors
		CAT_nuclear_weapons
		CAT_industry
		CAT_encryption_tech
		CAT_decryption_tech
		CAT_renewable
		CAT_computing_tech
		CAT_construction_tech
		CAT_internet_tech
		CAT_fuel_oil #Fuel
		CAT_excavation_tech ##Excavation
		CAT_infrastructure

	### Land doctrines
	CAT_land_doctrine		#For all land doctrines
		CAT_legacy_doctrines 	#For legacydoctrine
		CAT_western
		CAT_eastern
		CAT_offensive_doctrine
		CAT_defensive_doctrine
		CAT_unconventional
		CAT_equipment_doctrines
		CAT_training
		CAT_tech_doctrines


##############################################END OF EQUIPMENT###########################################

	### Doctrines ###
	CAT_air_doctrine #doctrine
	CAT_naval_doctrine

	CAT_battlefield_support_tree #doctrine
	CAT_operational_integrity_tree #doctrine
	CAT_battlefield_support #doctrine
	CAT_operational_integrity #doctrine

	CAT_green_water_navy #doctrine
	CAT_blue_water_navy #doctrine
	CAT_jeune_ecole_two # Doctrine

### MISSILES ###

	CAT_missile
	CAT_bm
	CAT_icbm
	CAT_irbm
	CAT_slbm
	CAT_cm
	CAT_alcm
	CAT_glcm
	CAT_slcm
	CAT_hscm
	CAT_sam
	CAT_abm
	CAT_space
	CAT_satellite
	CAT_gnss
	CAT_conventional_warhead
}

technology_folders = {
	infantry_folder = {
		ledger = army
	}
	armour_folder = {
		ledger = army
	}
	nsb_armour_folder = {
		ledger = army
		available = {
			has_dlc = "No Step Back"
		}
	}
	artillery_folder = {
		ledger = army
	}
	bba_aircraft_folder = {
		ledger = air
		available = {
			has_dlc = "By Blood Alone"
		}
	}
	air_techs_folder = {
		ledger = air
	}
	fixed_wing_folder = {
		ledger = air
		available = {
			NOT = { has_dlc = "By Blood Alone" }
		}
	}
	bomber_folder = {
		ledger = air
		available = {
			NOT = { has_dlc = "By Blood Alone" }
		}
	}
	#industry_folder
	land_doctrine_folder = {
		ledger = army
		doctrine = yes
	}
	naval_doctrine_folder = {
		ledger = navy
		doctrine = yes
	}
	air_doctrine_folder = {
		ledger = air
		doctrine = yes
	}
	special_forces_airborne_doctrine_folder = {
		ledger = hidden
		doctrine = yes
		available = {
			has_dlc = "Arms Against Tyranny"
		}
	}
	special_forces_airmobile_doctrine_folder = {
		ledger = hidden
		doctrine = yes
		available = {
			has_dlc = "Arms Against Tyranny"
		}
	}
	special_forces_marine_doctrine_folder = {
		ledger = hidden
		doctrine = yes
		available = {
			has_dlc = "Arms Against Tyranny"
		}
	}
	electronics_folder = {
		ledger = civilian
	}
	naval_folder = {
		available = {
			NOT = { has_dlc = "Man the Guns" }
		}
		ledger = navy
	}
	mtgnavalfolder = {
		available = {
			has_dlc = "Man the Guns"
		}
		ledger = navy
	}
	civilian_folder = {
		ledger = civilian
	}
	space_folder = {

	}
	ballistic_missiles_folder = {

	}
	cruise_missiles_folder = {

	}
	missile_defense_folder = {

	}
	warheads_folder = {

	}
	#strategic_tech_folder
	#secret_weapons_folder
}
