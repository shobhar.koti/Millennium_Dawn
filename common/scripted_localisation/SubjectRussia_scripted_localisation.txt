defined_text = {
	name = subject_campaign_loc
	text = {
		trigger = {
			check_variable = { SOV.subjectcampaign < 1 }
		}
		localization_key = subject_campaign_no_tt
	}
	text = {
		trigger = {
			check_variable = { SOV.subjectcampaign > 1 }
		}
		localization_key = subject_campaign_yes_tt
	}
}
defined_text = {
	name = subject_avtoritet_loc
	text = {
		trigger = {
			OR = {
				original_tag = CHE
				original_tag = SOO
				original_tag = CRM
				original_tag = YAK
				original_tag = DAG
				original_tag = KAE
				original_tag = TAT
				original_tag = BSH
				original_tag = NEE
				original_tag = YAM
				original_tag = CKK
				original_tag = KHS
				original_tag = ALT
				original_tag = URA
				original_tag = SIB
				original_tag = FAR
				original_tag = GOR
				original_tag = KUB
				original_tag = TUV
				original_tag = KOM
				original_tag = ADY
				original_tag = KBK
				original_tag = ING
				original_tag = BRY
				original_tag = KHM
				original_tag = KLM
				original_tag = DPR
				original_tag = HPR
				original_tag = OPR
				original_tag = LPR
				original_tag = MLR
				original_tag = PMR
			}
			has_autonomy_state = autonomy_republic_rf
			is_subject_of = SOV
		}
		localization_key = subject_avtoritet_locs
	}
}
defined_text = {
	name = SOV_TAT_separat
	text = {
		trigger = {
			TAT = {
				NOT = { has_country_flag = SUB_subject_rebeliion_flag }
				is_subject = yes
				has_autonomy_state = autonomy_republic_rf
			}
		}
		localization_key = SOV_TAT_separ_tt
	}
	text = {
		trigger = {
			NOT = {
				TAT = {
					is_subject = yes
					has_autonomy_state = autonomy_republic_rf
				}
			}
		}
		localization_key = ""
	}
}
defined_text = {
	name = SOV_BSH_separat
	text = {
		trigger = {
			BSH = {
				NOT = { has_country_flag = SUB_subject_rebeliion_flag }
				is_subject = yes
				has_autonomy_state = autonomy_republic_rf
			}
		}
		localization_key = SOV_BSH_separ_tt
	}
	text = {
		trigger = {
			NOT = {
				BSH = {
					is_subject = yes
					has_autonomy_state = autonomy_republic_rf
				}
			}
		}
		localization_key = ""
	}
}
defined_text = {
	name = SOV_CHE_separat
	text = {
		trigger = {
			CHE = {
				NOT = { has_country_flag = SUB_subject_rebeliion_flag }
				is_subject = yes
				has_autonomy_state = autonomy_republic_rf
			}
		}
		localization_key = SOV_CHE_separ_tt
	}
	text = {
		trigger = {
			NOT = {
				CHE = {
					is_subject = yes
					has_autonomy_state = autonomy_republic_rf
				}
			}
		}
		localization_key = ""
	}
}

