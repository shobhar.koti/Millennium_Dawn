on_actions = {
	on_startup = {
		effect = {
			###
		}
	}
	on_daily_GER = {
		effect = {
			GER = {
				if = {
					limit = {
						has_country_flag = GER_Final_settlement
						original_tag = GER
						has_army_manpower = { size > 220000 }
						NOT = { has_defensive_war = yes }
					}
					GER = {
						#country_lock_all_division_template = yes
						country_event = {
							id = Germany.561 days = 5 random_hours = 60
						}
					}
				}
			}
			#set_country_flag = GER_LEFTWING_DEPT_COLLAPSING
			#clr_country_flag = GER_LEFTWING_DEPT_STABLE
			#clr_country_flag = GER_LEFTWING_DEPT_UNSTABLE
		if = {
			limit = {
				has_country_flag = GER_LEFTWING_DEPT_COLLAPSING
			}
			set_country_flag = GER_LEFTISTS_ARE_RADICAL
			clr_country_flag = GER_LEFTISTS_ARE_CONTROLLED
			clr_country_flag = GER_LEFTISTS_ARE_ACTIVE
		}
		#
		if = {
			limit = {
				has_country_flag = GER_LEFTWING_DEPT_STABLE
			}
			set_country_flag = GER_LEFTISTS_ARE_CONTROLLED
			clr_country_flag = GER_LEFTISTS_ARE_RADICAL
			clr_country_flag = GER_LEFTISTS_ARE_ACTIVE
		}
		#
		if = {
			limit = {
				has_country_flag = GER_LEFTWING_DEPT_UNSTABLE
			}
			set_country_flag = GER_LEFTISTS_ARE_ACTIVE
			clr_country_flag = GER_LEFTISTS_ARE_CONTROLLED
			clr_country_flag = GER_LEFTISTS_ARE_RADICAL
		}
		#
		#########
			if = {
				limit = {
					has_country_flag = GER_RIGHTWING_DEPT_COLLAPSING
				}
				set_country_flag = GER_RIGHTISTS_ARE_RADICAL
				clr_country_flag = GER_RIGHTISTS_ARE_CONTROLLED
				clr_country_flag = GER_RIGHTISTS_ARE_ACTIVE
			}
			#
			if = {
				limit = {
					has_country_flag = GER_RIGHTWING_DEPT_STABLE
				}
				set_country_flag = GER_RIGHTISTS_ARE_CONTROLLED
				clr_country_flag = GER_RIGHTISTS_ARE_RADICAL
				clr_country_flag = GER_RIGHTISTS_ARE_ACTIVE
			}
			#
			if = {
				limit = {
					has_country_flag = GER_RIGHTWING_DEPT_UNSTABLE
				}
				set_country_flag = GER_RIGHTISTS_ARE_ACTIVE
				clr_country_flag = GER_RIGHTISTS_ARE_CONTROLLED
				clr_country_flag = GER_RIGHTISTS_ARE_RADICAL
			}
			# #
			if = {
				limit = {
					has_country_flag = GER_ISLAMIST_DEPT_COLLAPSING
				}
				set_country_flag = GER_ISLAMISTS_ARE_RADICAL
				clr_country_flag = GER_ISLAMISTS_ARE_CONTROLLED
				clr_country_flag = GER_ISLAMISTS_ARE_ACTIVE
			}
			#
			if = {
				limit = {
					has_country_flag = GER_ISLAMIST_DEPT_STABLE
				}
				set_country_flag = GER_ISLAMISTS_ARE_CONTROLLED
				clr_country_flag = GER_ISLAMISTS_ARE_RADICAL
				clr_country_flag = GER_ISLAMISTS_ARE_ACTIVE
			}
			#
			if = {
				limit = {
					has_country_flag = GER_ISLAMIST_DEPT_UNSTABLE
				}
				set_country_flag = GER_ISLAMISTS_ARE_ACTIVE
				clr_country_flag = GER_ISLAMISTS_ARE_CONTROLLED
				clr_country_flag = GER_ISLAMISTS_ARE_RADICAL
			}
			####
			if = {
				limit = {
					has_idea = GER_idea_paper_army
					check_variable = { bundeswehr_recovery_status > 99 }
				}
				remove_ideas = GER_idea_paper_army
				add_ideas = GER_idea_formidable_army
				clr_country_flag = GER_bundeswehr_in_need_of_reform
				country_event = {
					id = Germany.2343
				}
			}
			#
			if = {
				limit = {
					NOT = {
						has_country_flag = GER_LEFTWING_DEPT_COLLAPSED
					}
					check_variable = { GER_leftwing_department_status > 50 }
				}
				set_variable = { GER_leftwing_department_status = 50 }
			}
			if = { #If you hit 0, then the department collapses
				limit = {
					NOT = {
						has_country_flag = GER_LEFTWING_DEPT_COLLAPSED
					}
					check_variable = { GER_leftwing_department_status < 1 }
				}
				country_event = {
					id = Germany.234
					days = 1
				}
				set_country_flag = GER_LEFTWING_DEPT_COLLAPSED
				set_variable = { GER_leftwing_department_status = 0 }
				clr_country_flag = GER_LEFTWING_DEPT_STABLE
				clr_country_flag = GER_LEFTWING_DEPT_UNSTABLE
				clr_country_flag = GER_LEFTWING_DEPT_COLLAPSING
				remove_ideas = GER_idea_bfv_leftwing_low
				remove_ideas = GER_idea_bfv_leftwing_med
				remove_ideas = GER_idea_bfv_leftwing_high
			}
			#
			if = {
				limit = {
					NOT = {
						has_country_flag = GER_LEFTWING_DEPT_COLLAPSED
					}
					check_variable = { GER_leftwing_spending_level > 3 }
				}
				set_variable = { GER_leftwing_spending_level = 3 }
			}
			if = {
				limit = {
					NOT = {
						has_country_flag = GER_LEFTWING_DEPT_COLLAPSED
					}
					check_variable = { GER_leftwing_spending_level < 1 }
				}
				set_variable = { GER_leftwing_spending_level = 1 }
			}
			if = {
				limit = {
					NOT = {
						has_country_flag = GER_LEFTWING_DEPT_COLLAPSED
					}
					check_variable = { GER_leftwing_spending_level = 1 }
				}
				add_ideas = GER_idea_bfv_leftwing_low
				remove_ideas = GER_idea_bfv_leftwing_med
				remove_ideas = GER_idea_bfv_leftwing_high
			}
			if = {
				limit = {
					NOT = {
						has_country_flag = GER_LEFTWING_DEPT_COLLAPSED
					}
					check_variable = { GER_leftwing_spending_level = 2 }
				}
				add_ideas = GER_idea_bfv_leftwing_med
				remove_ideas = GER_idea_bfv_leftwing_low
				remove_ideas = GER_idea_bfv_leftwing_high
			}
			if = {
				limit = {
					NOT = {
						has_country_flag = GER_LEFTWING_DEPT_COLLAPSED
					}
					check_variable = { GER_leftwing_spending_level = 3 }
				}
				add_ideas = GER_idea_bfv_leftwing_high
				remove_ideas = GER_idea_bfv_leftwing_med
				remove_ideas = GER_idea_bfv_leftwing_low
			}
			if = {
				limit = {
					NOT = {
						has_country_flag = GER_LEFTWING_DEPT_COLLAPSED
					}
					check_variable = { GER_leftwing_department_status > 30 }
				}
				set_country_flag = GER_LEFTWING_DEPT_STABLE
				clr_country_flag = GER_LEFTWING_DEPT_UNSTABLE
				clr_country_flag = GER_LEFTWING_DEPT_COLLAPSING
			}
			if = {
				limit = {
					NOT = {
						has_country_flag = GER_LEFTWING_DEPT_COLLAPSED
					}
					check_variable = { GER_leftwing_department_status < 30 }
					check_variable = { GER_leftwing_department_status > 10 }
				}
				set_country_flag = GER_LEFTWING_DEPT_UNSTABLE
				clr_country_flag = GER_LEFTWING_DEPT_STABLE
				clr_country_flag = GER_LEFTWING_DEPT_COLLAPSING
			}
			if = {
				limit = {
					NOT = {
						has_country_flag = GER_LEFTWING_DEPT_COLLAPSED
					}
					check_variable = { GER_leftwing_department_status < 10 }
				}
				set_country_flag = GER_LEFTWING_DEPT_COLLAPSING
				clr_country_flag = GER_LEFTWING_DEPT_STABLE
				clr_country_flag = GER_LEFTWING_DEPT_UNSTABLE
			}
			#####
			if = {
				limit = {
					NOT = {
						has_country_flag = GER_RIGHTWING_DEPT_COLLAPSED
					}
					check_variable = { GER_rightwing_department_status > 50 }
				}
				set_variable = { GER_rightwing_department_status = 50 }
			}
			if = { #If you hit 0, then the department collapses
				limit = {
					NOT = {
						has_country_flag = GER_RIGHTWING_DEPT_COLLAPSED
					}
					check_variable = { GER_rightwing_department_status < 1 }
				}
				country_event = {
					id = Germany.234
					days = 1
				}
				set_country_flag = GER_RIGHTWING_DEPT_COLLAPSED
				set_variable = { GER_rightwing_department_status = 0 }
				clr_country_flag = GER_RIGHTWING_DEPT_STABLE
				clr_country_flag = GER_RIGHTWING_DEPT_UNSTABLE
				clr_country_flag = GER_RIGHTWING_DEPT_COLLAPSING
				remove_ideas = GER_idea_bfv_rightwing_low
				remove_ideas = GER_idea_bfv_rightwing_med
				remove_ideas = GER_idea_bfv_rightwing_high
			}
			#
			if = {
				limit = {
					NOT = {
						has_country_flag = GER_RIGHTWING_DEPT_COLLAPSED
					}
					check_variable = { GER_rightwing_spending_level > 3 }
				}
				set_variable = { GER_rightwing_spending_level = 3 }
			}
			if = {
				limit = {
					NOT = {
						has_country_flag = GER_RIGHTWING_DEPT_COLLAPSED
					}
					check_variable = { GER_rightwing_spending_level < 1 }
				}
				set_variable = { GER_rightwing_spending_level = 1 }
			}
			if = {
				limit = {
					NOT = {
						has_country_flag = GER_RIGHTWING_DEPT_COLLAPSED
					}
					check_variable = { GER_rightwing_spending_level = 1 }
				}
				add_ideas = GER_idea_bfv_rightwing_low
				remove_ideas = GER_idea_bfv_rightwing_med
				remove_ideas = GER_idea_bfv_rightwing_high
			}
			if = {
				limit = {
					NOT = {
						has_country_flag = GER_RIGHTWING_DEPT_COLLAPSED
					}
					check_variable = { GER_rightwing_spending_level = 2 }
				}
				add_ideas = GER_idea_bfv_rightwing_med
				remove_ideas = GER_idea_bfv_rightwing_low
				remove_ideas = GER_idea_bfv_rightwing_high
			}
			if = {
				limit = {
					NOT = {
						has_country_flag = GER_RIGHTWING_DEPT_COLLAPSED
					}
					check_variable = { GER_rightwing_spending_level = 3 }
				}
				add_ideas = GER_idea_bfv_rightwing_high
				remove_ideas = GER_idea_bfv_rightwing_med
				remove_ideas = GER_idea_bfv_rightwing_low
			}
			if = {
				limit = {
					NOT = {
						has_country_flag = GER_RIGHTWING_DEPT_COLLAPSED
					}
					check_variable = { GER_rightwing_department_status > 30 }
				}
				set_country_flag = GER_RIGHTWING_DEPT_STABLE
				clr_country_flag = GER_RIGHTWING_DEPT_UNSTABLE
				clr_country_flag = GER_RIGHTWING_DEPT_COLLAPSING
			}
			if = {
				limit = {
					NOT = {
						has_country_flag = GER_RIGHTWING_DEPT_COLLAPSED
					}
					check_variable = { GER_rightwing_department_status < 30 }
					check_variable = { GER_rightwing_department_status > 10 }
				}
				set_country_flag = GER_RIGHTWING_DEPT_UNSTABLE
				clr_country_flag = GER_RIGHTWING_DEPT_STABLE
				clr_country_flag = GER_RIGHTWING_DEPT_COLLAPSING
			}
			if = {
				limit = {
					NOT = {
						has_country_flag = GER_RIGHTWING_DEPT_COLLAPSED
					}
					check_variable = { GER_rightwing_department_status < 10 }
				}
				set_country_flag = GER_RIGHTWING_DEPT_COLLAPSING
				clr_country_flag = GER_RIGHTWING_DEPT_STABLE
				clr_country_flag = GER_RIGHTWING_DEPT_UNSTABLE
			}
			# # #
			# Below 10 - Collapsing - 20 to 30 - Unstable - 30+ Stable
			if = {
				limit = {
					NOT = {
						has_country_flag = GER_ISLAMIST_DEPT_COLLAPSED
					}
					check_variable = { GER_islamist_department_status > 50 }
				}
				set_variable = { GER_islamist_department_status = 50 }
			}
			if = { #If you hit 0, then the department collapses
				limit = {
					NOT = {
						has_country_flag = GER_ISLAMIST_DEPT_COLLAPSED
					}
					check_variable = { GER_islamist_department_status < 1 }
				}
				set_country_flag = GER_ISLAMIST_DEPT_COLLAPSED
				set_variable = { GER_islamist_department_status = 0 }
				clr_country_flag = GER_ISLAMIST_DEPT_STABLE
				clr_country_flag = GER_ISLAMIST_DEPT_UNSTABLE
				clr_country_flag = GER_ISLAMIST_DEPT_COLLAPSING
				remove_ideas = GER_idea_bfv_islamist_low
				remove_ideas = GER_idea_bfv_islamist_med
				remove_ideas = GER_idea_bfv_islamist_high
			}
			#
			if = {
				limit = {
					NOT = {
						has_country_flag = GER_ISLAMIST_DEPT_COLLAPSED
					}
					check_variable = { GER_islamist_spending_level > 3 }
				}
				set_variable = { GER_islamist_spending_level = 3 }
			}
			if = {
				limit = {
					NOT = {
						has_country_flag = GER_ISLAMIST_DEPT_COLLAPSED
					}
					check_variable = { GER_islamist_spending_level < 1 }
				}
				set_variable = { GER_islamist_spending_level = 1 }
			}
			if = {
				limit = {
					NOT = {
						has_country_flag = GER_ISLAMIST_DEPT_COLLAPSED
					}
					check_variable = { GER_islamist_spending_level = 1 }
				}
				add_ideas = GER_idea_bfv_islamist_low
				remove_ideas = GER_idea_bfv_islamist_med
				remove_ideas = GER_idea_bfv_islamist_high
			}
			if = {
				limit = {
					NOT = {
						has_country_flag = GER_ISLAMIST_DEPT_COLLAPSED
					}
					check_variable = { GER_islamist_spending_level = 2 }
				}
				add_ideas = GER_idea_bfv_islamist_med
				remove_ideas = GER_idea_bfv_islamist_low
				remove_ideas = GER_idea_bfv_islamist_high
			}
			if = {
				limit = {
					NOT = {
						has_country_flag = GER_ISLAMIST_DEPT_COLLAPSED
					}
					check_variable = { GER_islamist_spending_level = 3 }
				}
				add_ideas = GER_idea_bfv_islamist_high
				remove_ideas = GER_idea_bfv_islamist_med
				remove_ideas = GER_idea_bfv_islamist_low
			}
			# # #
			if = {
				limit = {
					NOT = {
						has_country_flag = GER_ISLAMIST_DEPT_COLLAPSED
					}
					check_variable = { GER_islamist_department_status > 30 }
				}
				set_country_flag = GER_ISLAMIST_DEPT_STABLE
				clr_country_flag = GER_ISLAMIST_DEPT_UNSTABLE
				clr_country_flag = GER_ISLAMIST_DEPT_COLLAPSING
			}
			if = {
				limit = {
					NOT = {
						has_country_flag = GER_ISLAMIST_DEPT_COLLAPSED
					}
					check_variable = { GER_islamist_department_status < 30 }
					check_variable = { GER_islamist_department_status > 10 }
				}
				set_country_flag = GER_ISLAMIST_DEPT_UNSTABLE
				clr_country_flag = GER_ISLAMIST_DEPT_STABLE
				clr_country_flag = GER_ISLAMIST_DEPT_COLLAPSING
			}
			if = {
				limit = {
					NOT = {
						has_country_flag = GER_ISLAMIST_DEPT_COLLAPSED
					}
					check_variable = { GER_islamist_department_status < 10 }
				}
				set_country_flag = GER_ISLAMIST_DEPT_COLLAPSING
				clr_country_flag = GER_ISLAMIST_DEPT_STABLE
				clr_country_flag = GER_ISLAMIST_DEPT_UNSTABLE
			}
			#
			if = {
				limit = {
					NOT = {
						OR = {
							has_country_flag = GER_RIGHTWING_DEPT_COLLAPSED
							has_country_flag = GER_LEFTWING_DEPT_COLLAPSED
						}
					}
					OR = {
						check_variable = { GER_rightwing_department_status < 20 }
						check_variable = { GER_leftwing_department_status < 20 }
						check_variable = { GER_islamist_department_status < 20 }
					}
				}
				country_event = {
					id = Germany.238
				}
			}
			#
			# # #
			# EAST & WEST DIVIDE MECHANIC
			if = {
				limit = {
					check_variable = { GER_east_opinion > 30 }
				}
				remove_ideas = GER_idea_east_german_debuff2
				remove_ideas = GER_idea_east_german_debuff4
				remove_ideas = GER_idea_east_german_debuff3
				remove_ideas = GER_idea_east_german_debuff1
			}
			#
			if = {
				limit = {
					check_variable = { GER_east_opinion < 30 }
					check_variable = { GER_east_opinion > 24 }
				}
				add_ideas = GER_idea_east_german_debuff1
				remove_ideas = GER_idea_east_german_debuff2
				remove_ideas = GER_idea_east_german_debuff4
				remove_ideas = GER_idea_east_german_debuff3
			}
			if = {
				limit = {
					check_variable = { GER_east_opinion < 26 }
					check_variable = { GER_east_opinion > 19 }
				}
				add_ideas = GER_idea_east_german_debuff2
				remove_ideas = GER_idea_east_german_debuff1
				remove_ideas = GER_idea_east_german_debuff4
				remove_ideas = GER_idea_east_german_debuff3
			}
			if = {
				limit = {
					check_variable = { GER_east_opinion < 20 }
					check_variable = { GER_east_opinion > 14 }
				}
				add_ideas = GER_idea_east_german_debuff3
				remove_ideas = GER_idea_east_german_debuff2
				remove_ideas = GER_idea_east_german_debuff1
				remove_ideas = GER_idea_east_german_debuff4
			}
			if = {
				limit = {
					check_variable = { GER_east_opinion < 14 }
				}
				add_ideas = GER_idea_east_german_debuff4
				remove_ideas = GER_idea_east_german_debuff3
				remove_ideas = GER_idea_east_german_debuff2
				remove_ideas = GER_idea_east_german_debuff1
			}
			##########
			if = {
				limit = {
					check_variable = { GER_soldiers_popularity_mideast < 49 }
					has_country_flag = GER_war_on_terror_started
				}
				remove_ideas = GER_idea_afghan_bonus_1
				remove_ideas = GER_idea_afghan_bonus_2
				remove_ideas = GER_idea_afghan_bonus_3
			}
			if = {
				limit = {
					check_variable = { GER_soldiers_popularity_mideast > 49 }
					check_variable = { GER_soldiers_popularity_mideast < 69 }
					has_country_flag = GER_war_on_terror_started
				}
				add_ideas = GER_idea_afghan_bonus_1
				remove_ideas = GER_idea_afghan_bonus_2
				remove_ideas = GER_idea_afghan_bonus_3
			}
			#
			if = {
				limit = {
					check_variable = { GER_soldiers_popularity_mideast > 69 }
					check_variable = { GER_soldiers_popularity_mideast < 89 }
					has_country_flag = GER_war_on_terror_started
				}
				add_ideas = GER_idea_afghan_bonus_2
				remove_ideas = GER_idea_afghan_bonus_1
				remove_ideas = GER_idea_afghan_bonus_3
			}
			#
			if = {
				limit = {
					check_variable = { GER_soldiers_popularity_mideast > 89 }
					has_country_flag = GER_war_on_terror_started
				}
				add_ideas = GER_idea_afghan_bonus_3
				remove_ideas = GER_idea_afghan_bonus_2
				remove_ideas = GER_idea_afghan_bonus_1
			}
		}
	}
	on_weekly_GER = {
		effect = {
			# Bavaria system
			if = {
				limit = {
					NOT = {
						is_ai = yes
						has_country_flag = GER_bavaria_gone
					}
				}
				GER_update_bavaria_standing = yes
			}
			if = {
				limit = {
					check_variable = { GER_bavarian_nationalism_var > 99 }
				}
				country_event = {
					id = bavaria.1
				}
			}
			if = {
				limit = {
					check_variable = { GER_bavarian_nationalism_var > 100 }
				}
				set_variable = { GER_bavarian_nationalism_var = 100 }
			}
			if = {
				limit = {
					check_variable = { GER_bavarian_nationalism_var < 0 }
				}
				set_variable = { GER_bavarian_nationalism_var = 0 }
			}
			if = {
				limit = {
					check_variable = { GER_east_opinion > 40 }
					NOT = {
						has_country_flag = GER_united
					}
				}
				GER_east_german_events = yes
			}
			if = {
				limit = {
					check_variable = { GER_east_opinion < 41 }
					NOT = {
						has_country_flag = GER_united
					}
				}
				GER_die_linke_events = yes
			}
			if = {
				limit = {
					check_variable = { GER_east_opinion > 50 }
				}
				set_variable = { GER_east_opinion = 50 }
			}
			if = {
				limit = {
					check_variable = { GER_east_opinion < 0 }
				}
				set_variable = { GER_east_opinion = 0 }
			}
		}
	}
	on_monthly_GER = {
		effect = {
			if = {
				limit = {
					has_country_flag = GER_war_on_terror_started
				}
				random_list = {
					50 = {
					}
					10 = {
						add_to_variable = { var = GER_soldiers_popularity_mideast value = -1 }
						clamp_variable = { var = GER_soldiers_popularity_mideast min = 0 max = 100 }
					}
					10 = {
						add_to_variable = { var = GER_soldiers_popularity_mideast value = -2 }
						clamp_variable = { var = GER_soldiers_popularity_mideast min = 0 max = 100 }
					}
					10 = {
						add_to_variable = { var = GER_soldiers_popularity_mideast value = -3 }
						clamp_variable = { var = GER_soldiers_popularity_mideast min = 0 max = 100 }
					}
					10 = {
						add_to_variable = { var = GER_soldiers_popularity_mideast value = -4 }
						clamp_variable = { var = GER_soldiers_popularity_mideast min = 0 max = 100 }
					}
					10 = {
						add_to_variable = { var = GER_soldiers_popularity_mideast value = -5 }
						clamp_variable = { var = GER_soldiers_popularity_mideast min = 0 max = 100 }
					}
				}
			}
			# BfV system
			if = {
				limit = {
					NOT = {
						OR = {
							has_country_flag = GER_LEFTWING_DEPT_COLLAPSED
							has_country_flag = GER_ISLAMIST_DEPT_COLLAPSED
							has_country_flag = GER_RIGHTWING_DEPT_COLLAPSED
						}
					}
				}
				GER_bfv_events = yes
			}
			if = {
				limit = {
					has_country_flag = GER_war_on_terror_started
				}
				GER_wot_random_events = yes
			}
			if = {
				limit = {
					check_variable = { GER_islamist_spending_level = 1 }
				}
				random_list = {
					50 = {
					}
					15 = {
						add_to_variable = { GER_islamist_department_status = -1 }
					}
					15 = {
						add_to_variable = { GER_islamist_department_status = -2 }
					}
					10 = {
						add_to_variable = { GER_islamist_department_status = -3 }
					}
					10 = {
						add_to_variable = { GER_islamist_department_status = -5 }
					}
				}
			}
			if = {
				limit = {
					check_variable = { GER_rightwing_spending_level = 1 }
				}
				random_list = {
					50 = {
					}
					15 = {
						add_to_variable = { GER_rightwing_department_status = -1 }
					}
					15 = {
						add_to_variable = { GER_rightwing_department_status = -2 }
					}
					10 = {
						add_to_variable = { GER_rightwing_department_status = -3 }
					}
					10 = {
						add_to_variable = { GER_rightwing_department_status = -5 }
					}
				}
			}
			if = {
				limit = {
					check_variable = { GER_leftwing_spending_level = 1 }
				}
				random_list = {
					50 = {
					}
					15 = {
						add_to_variable = { GER_leftwing_department_status = -1 }
					}
					15 = {
						add_to_variable = { GER_leftwing_department_status = -2 }
					}
					10 = {
						add_to_variable = { GER_leftwing_department_status = -3 }
					}
					10 = {
						add_to_variable = { GER_leftwing_department_status = -5 }
					}
				}
			}
			if = {
				limit = {
					check_variable = { GER_leftwing_spending_level = 3 }
				}
				random_list = {
					60 = {
					}
					20 = {
						add_to_variable = { GER_leftwing_department_status = 2 }
					}
					20 = {
						add_to_variable = { GER_leftwing_department_status = 3 }
					}
				}
			}
			if = {
				limit = {
					check_variable = { GER_rightwing_spending_level = 3 }
				}
				random_list = {
					60 = {
					}
					20 = {
						add_to_variable = { GER_rightwing_department_status = 2 }
					}
					20 = {
						add_to_variable = { GER_rightwing_department_status = 3 }
					}
				}
			}
			if = {
				limit = {
					check_variable = { GER_islamist_spending_level = 3 }
				}
				random_list = {
					60 = {
					}
					20 = {
						add_to_variable = { GER_islamist_department_status = 2 }
					}
					20 = {
						add_to_variable = { GER_islamist_department_status = 3 }
					}
				}
			}
		}
	}
}
