CES_liberals_modifier = {
	enable = { always = yes }
	icon = GFX_idea_democracy
	remove_trigger = {
		NOT = {
			has_government = democratic
		}
	}
	drift_defence_factor = CES_liberals_drift_defence_factor
	democratic_drift = CES_liberals_democratic_support
	bureaucracy_cost_multiplier_modifier = CES_liberals_bureaucracy_cost_multiplier_modifier
	stability_factor = CES_liberals_stability_factor
	local_resources_factor = CES_liberals_local_resources_factor
	social_cost_multiplier_modifier = CES_liberals_social_cost_multiplier_modifier
	health_cost_multiplier_modifier = CES_liberals_health_cost_multiplier_modifier
	nationalist_drift = CES_liberals_nationalist_drift
	communism_drift = CES_liberals_communism_drift
	monthly_population = CES_liberals_monthly_population
	research_speed_factor = CES_liberals_research_speed_factor
	education_cost_multiplier_modifier = CES_liberals_education_budget_cost_factor
}

CES_agriculture_policy = {
	icon = "GFX_idea_agrarian_economy_idea"
	enable = {
		always = yes
	}
	production_speed_buildings_factor = CES_agriculture_production_speed_buildings_factor
	social_cost_multiplier_modifier = CES_agriculture_social_cost_multiplier_modifier
	min_export = CES_agriculture_min_export
	stability_factor = CES_agriculture_stability_factor
	tax_gain_multiplier_modifier = CES_agriculture_tax_gain_multiplier_modifier
	political_power_factor = CES_agriculture_political_power_factor
	production_factory_efficiency_gain_factor = CES_agriculture_production_factory_efficiency_gain_factor
}

CES_nationalist_policy = {
	icon = "GFX_idea_nationalism"
	enable = {
		always = yes
	}
	remove_trigger = {
		NOT = {
			has_government = nationalist
		}
	}
	nationalist_drift = CES_nationalist_polic_support
	communism_drift = CES_communism_polic_support
	democratic_drift = CES_democratic_polic_support
	political_power_factor = CES_nationalist_policy_political_power_factor
	army_attack_factor = CES_nationalist_policy_army_attack_factor
	army_org_factor = CES_nationalist_policy_army_org_factor
	army_speed_factor = CES_nationalist_policy_army_speed_factor
	recruitable_population_factor = CES_nationalist_policy_recruitable_population_factor
}

CES_kaz_modifier = {
	icon = "GFX_idea_ces_kaz_reforms"
	enable = {
		original_tag = KAZ
		has_completed_focus = CES_kaz_reform_vs
	}
	encryption_factor = CES_kaz_encryption_factor
	army_core_defence_factor = CES_kaz_army_core_defence_factor
	resistance_growth = CES_kaz_police_resistance_growth
	police_cost_multiplier_modifier = CES_kaz_police_cost_multiplier_modifier
	foreign_influence_defense_modifier = CES_kaz_foreign_influence_defense_modifier
	personnel_cost_multiplier_modifier = CES_kaz_personnel_cost_multiplier_modifier
	army_defence_factor = CES_kaz_army_defence_factor
	army_org_factor = CES_kaz_personnel_army_org_factor
	army_attack_factor = CES_kaz_army_attack_factor
	training_time_army_factor = CES_kaz_training_time_army_factor
}

CES_jihadism = {
	icon = "GFX_idea_army_of_aggression"
	enable = {
		always = yes
	}
	remove_trigger = {
		NOT = {
			has_government = fascism
		}
	}
	stability_factor = CES_jihadism_stability_factor
	fascism_drift = CES_fascism_jihadism_support
	communism_drift = CES_communism_jihadism_support
	democratic_drift = CES_democratic_jihadism_support
	nationalist_drift = CES_nationalist_jihadism_support
	army_attack_factor = CES_jihadism_army_attack_factor
	army_defence_factor = CES_jihadism_army_defence_factor
	army_org_factor = CES_jihadism_army_org_factor
	recruitable_population_factor = CES_jihadism_recruitable_population_factor
	fascism_acceptance = CES_jihadism_fascism_acceptance
	democratic_acceptance = CES_jihadism_democratic_acceptance
	nationalist_acceptance = CES_jihadism_nationalist_acceptance
	communism_acceptance = CES_jihadism_communism_acceptance
}

CES_red_zaraza = {
	icon = "GFX_idea_army_of_aggression"
	remove_trigger = {
		NOT = {
			OR = {
				emerging_communist_state_are_in_power = yes
				emerging_anarchist_communism_are_in_power = yes
				neutrality_neutral_communism_are_in_power = yes
			}
		}
	}
	consumer_goods_factor = CES_red_zaraza_consumer_goods_factor
	stability_factor = CES_red_zaraza_stability_factor
	political_power_factor = CES_red_zaraza_political_power_factor
	production_speed_arms_factory_factor = CES_red_zaraza_production_speed_arms_factory_factor
	political_power_factor = CES_red_zaraza_political_power_factor
	recruitable_population_factor = CES_red_zaraza_recruitable_population_factor
	drift_defence_factor = CES_red_zaraza_modifier_drift_defence_fact
	communism_drift = CES_red_zaraza_modifier_communism_drift
	research_speed_factor = CES_red_zaraza_research_speed_factor
	monthly_population = CES_red_zaraza_modifier_monthly_population
	education_cost_multiplier_modifier = CES_red_zaraza_education_budget_cost_factor
}

CES_policy = {
	icon = "GFX_idea_army_of_aggression"
	remove_trigger = {
		NOT = {
			OR = {
				emerging_communist_state_are_in_power = yes
				emerging_anarchist_communism_are_in_power = yes
				neutrality_neutral_communism_are_in_power = yes
			}
		}
	}
	
}