set_leader_USA = {

	if = { limit = { has_country_flag = set_conservatism }
		if = { limit = { check_variable = { conservatism_leader = 0 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "George W. Bush"
				picture = "George_W_Bush.dds"
				desc = "GEORGE_BUSH_LEADER_DESC"
				ideology = conservatism
				traits = {
					western_conservatism
					businessman
					rational
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { conservatism_leader = 1 } }
			if = { limit = { date < 2001.2.2 is_in_array = { ruling_party = 2 } } set_temp_variable = { b = 1 } } #Lost The election and future Party nomination
			if = { limit = { date < 2005.1.1 } set_temp_variable = { b = 1 } } #Skip if past his irl terms
		}
		if = { limit = { check_variable = { conservatism_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "John McCain"
				picture = "john_mccain.dds"
				ideology = conservatism
				traits = {
					western_conservatism
					military_career
					career_politician
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { conservatism_leader = 1 } }
			if = { limit = { date < 2009.2.2 is_in_array = { ruling_party = 2 } } set_temp_variable = { b = 1 } } #Lost The election and future Party nomination
			if = { limit = { date < 2013.1.2 } set_temp_variable = { b = 1 } } #Incumbant Election possibility "two term period"
		}
		if = { limit = { check_variable = { conservatism_leader = 2 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Mitt Romney"
				picture = "Mitt_Romney.dds"
				desc = "MITT_ROMNEY_LEADER_DESC"
				ideology = conservatism
				traits = {
					western_conservatism
					businessman
					rational
					lawyer
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { conservatism_leader = 1 } }
			if = { limit = { date < 2013.2.2 is_in_array = { ruling_party = 2 } } set_temp_variable = { b = 1 } } #Lost The election and future Party nomination
			if = { limit = { date < 2017.1.2 } set_temp_variable = { b = 1 } } #Incumbant Election possibility "two term period"
		}
		if = { limit = { check_variable = { conservatism_leader = 3 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Donald Trump"
				desc = "DONALD_TRUMP_LEADER_DESC"
				picture = "Donald_Trump.dds"
				ideology = conservatism
				traits = {
					western_conservatism
					businessman
					narcissist
					greedy
					emotional
				}
			}
			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { conservatism_leader = 1 } }
			if = { limit = { date < 2017.2.2 is_in_array = { ruling_party = 2 } } set_temp_variable = { b = 1 } } #Lost The election and future Party nomination
			if = { limit = { date < 2021.1.2 } set_temp_variable = { b = 1 } } #Incumbant Election possibility "two term period"
		}
		if = { limit = { check_variable = { conservatism_leader = 4 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Marco Rubio"
				picture = "Rubio_Marco.dds"
				ideology = conservatism
				traits = {
					western_conservatism
				}
			}


			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { conservatism_leader = 1 } }
		}
	}

	############ Democratic Party Leaders ####################

	else_if = { limit = { has_country_flag = set_liberalism }
		if = { limit = { check_variable = { liberalism_leader = 0 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { liberalism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Al Gore"
				picture = "al_gore.dds"
				ideology = liberalism
				traits = {
					western_liberalism
					party_trait_green
					career_politician
					rational
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { liberalism_leader = 1 } }
			if = { limit = { date < 2001.2.2 is_in_array = { ruling_party = 1 } } set_temp_variable = { b = 1 } } #Lost The election and future Party nomination
			if = { limit = { date < 2005.1.1 } set_temp_variable = { b = 1 } } #Skip if past the "two term limit"
		}
		if = { limit = { check_variable = { liberalism_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { liberalism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Barack Obama"
				desc = "BARACK_OBAMA_LEADER_DESC"
				picture = "Barack_Obama.dds"
				ideology = liberalism
				traits = {
					lawyer
					western_liberalism
					polished
					honest
					rational
					geopolitical_thinker
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { liberalism_leader = 1 } }
			if = { limit = { date < 2009.2.2 is_in_array = { ruling_party = 1 } } set_temp_variable = { b = 1 } } #Lost The election and future Party nomination
			if = { limit = { date < 2013.1.1 } set_temp_variable = { b = 1 } } #Skip if past the "two term limit"
		}
		if = { limit = { check_variable = { liberalism_leader = 2 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { liberalism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Hillary Clinton"
				picture = "Hillary_Clinton.dds"
				ideology = liberalism
				traits = {
					career_politician
					western_liberalism
					polished
					greedy
					rational
					political_dancer
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { liberalism_leader = 1 } }
			if = { limit = { date < 2017.2.2 is_in_array = { ruling_party = 1 } } set_temp_variable = { b = 1 } } #Lost The election and future Party nomination
			if = { limit = { date < 2021.1.1 } set_temp_variable = { b = 1 } } #Skip if past the "two term limit"
		}
		if = { limit = { check_variable = { liberalism_leader = 3 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { liberalism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Joe Biden"
				picture = "Joe_Biden.dds"
				ideology = liberalism
				traits = {
					western_liberalism
					career_politician
					polished
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { liberalism_leader = 1 } }
			if = { limit = { date < 2021.2.2 is_in_array = { ruling_party = 1 } } set_temp_variable = { b = 1 } } #Lost The election and future Party nomination
			if = { limit = { date < 2025.1.1 } set_temp_variable = { b = 1 } } #Skip if past the "two term limit"
		}
		if = { limit = { check_variable = { liberalism_leader = 4 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { liberalism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Tim Kaine"
				picture = "Tim_Kaine.dds"
				ideology = liberalism
				traits = {
					western_liberalism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { liberalism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { liberalism_leader = 5 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { liberalism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Cory Booker"
				picture = "Cory_Booker.dds"
				ideology = liberalism
				traits = {
					western_liberalism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { liberalism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { liberalism_leader = 7 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { liberalism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Kanye West"
				picture = "Kanye_West.dds"
				ideology = liberalism
				traits = {
					western_liberalism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { liberalism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { liberalism_leader = 8 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { liberalism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Shawn C. Carter"
				picture = "Shawn_C_Carter.dds"
				ideology = liberalism
				traits = {
					western_liberalism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { liberalism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}

	############ Progressive Party Leaders ####################

	else_if = { limit = { has_country_flag = set_socialism }
		if = { limit = { check_variable = { socialism_leader = 0 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { socialism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Dennis Kucinich"
				picture = "dennis_kucinich.dds"
				ideology = socialism
				traits = {
					western_socialism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { socialism_leader = 1 } }
		}
		if = { limit = { check_variable = { socialism_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { socialism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Elizabeth Warren"
				picture = "Elizabeth_Warren.dds"
				ideology = socialism
				traits = {
					western_socialism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { socialism_leader = 1 } }
		}
	}

	############ Shepard's Party Leaders ####################

	else_if = { limit = { has_country_flag = set_Communist-State }
		if = { limit = { check_variable = { Communist-State_leader = 0 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Communist-State_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Sam Webb"
				picture = "Sam_Webb.dds"
				ideology = Communist-State
				traits = {
					emerging_Communist-State
					rational
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Communist-State_leader = 1 } }
		}
		if = { limit = { check_variable = { Communist-State_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Communist-State_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "John Bachtell"
				picture = "John_Bachtell.dds"
				ideology = Communist-State
				traits = {
					emerging_Communist-State
					honest
					humble
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Communist-State_leader = 1 } }
		}
	}


	else_if = { limit = { has_country_flag = set_Conservative }
		if = { limit = { check_variable = { Conservative_leader = 0 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Conservative_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Johnathan Hill"
				picture = "generic.dds"
				ideology = Conservative
				traits = {
					emerging_Conservative
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Conservative_leader = 1 } }
		}
	}


	else_if = { limit = { has_country_flag = set_Autocracy }
		if = { limit = { check_variable = { Autocracy_leader = 0 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Council For National Preservation"
				picture = "Council_For_National_Preservation.dds"
				desc = "COUNCIL_FOR_NATIONAL_PRESERVATION_DESC"
				ideology = Autocracy
				traits = {
					emerging_Autocracy
					defender_of_democracy
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Autocracy_leader = 1 } }
		}
	}


	else_if = { limit = { has_country_flag = set_anarchist_communism }
		if = { limit = { check_variable = { anarchist_communism_leader = 0 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { anarchist_communism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Central Committee"
				picture = "generic.dds"
				ideology = anarchist_communism
				traits = {
					emerging_anarchist_communism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { anarchist_communism_leader = 1 } }
		}
	}


	else_if = { limit = { has_country_flag = set_Neutral_conservatism }
		if = { limit = { check_variable = { Neutral_conservatism_leader = 0 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Neutral_conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Chris Cox"
				picture = "chris_cox.dds"
				ideology = Neutral_conservatism
				traits = {
					neutrality_Neutral_conservatism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_conservatism_leader = 1 } }
		}
		if = { limit = { check_variable = { Neutral_conservatism_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Neutral_conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "John Kasich"
				picture = "john_kasich.dds"
				ideology = Neutral_conservatism
				traits = {
					neutrality_Neutral_conservatism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_conservatism_leader = 1 } }
		}
	}


	else_if = { limit = { has_country_flag = set_neutral_Social }
		if = { limit = { check_variable = { neutral_Social_leader = 0 } }
			add_to_variable = { neutral_Social_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Bernie Sanders"
				picture = "Bernie_Sanders.dds"
				ideology = neutral_Social
				traits = {
					neutrality_neutral_Social
					career_politician
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { neutral_Social_leader = 1 } }
		}
	}

		############ Neutral Autocracy Leaders ####################

	else_if = { limit = { has_country_flag = set_Neutral_Autocracy }
		if = { limit = { check_variable = { Neutral_Autocracy_leader = 0 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Neutral_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Pat Buchanan"
				picture = "Pat_Buchanan.dds"
				ideology = Neutral_Autocracy
				traits = {
					neutrality_Neutral_Autocracy
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Autocracy_leader = 1 } }
		}
	}


	else_if = { limit = { has_country_flag = set_Neutral_Communism }
		if = { limit = { check_variable = { Neutral_Communism_leader = 0 } }
			add_to_variable = { Neutral_Communism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Larry Holmes"
				picture = "Larry_Golmes.dds"
				ideology = Neutral_Communism
				traits = {
					neutrality_Neutral_Communism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Communism_leader = 1 } }
		}
	}
	else_if = { limit = { has_country_flag = set_Neutral_Muslim_Brotherhood }
		if = { limit = { check_variable = { Neutral_Muslim_Brotherhood_leader = 0 } }
			add_to_variable = { Neutral_Muslim_Brotherhood_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Louis Farrakhan"
				picture = "Louis_Farrakhan.dds"
				ideology = Neutral_Muslim_Brotherhood
				traits = {
					neutrality_Neutral_Muslim_Brotherhood
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Muslim_Brotherhood_leader = 1 } }
		}
		if = { limit = { check_variable = { Neutral_Muslim_Brotherhood_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Neutral_Muslim_Brotherhood_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Nihad Awad"
				picture = "nihad_awad.dds"
				ideology = Neutral_Muslim_Brotherhood
				traits = {
					neutrality_Neutral_Muslim_Brotherhood
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Muslim_Brotherhood_leader = 1 } }
		}
	}

	############ Libertarian Party Leaders ####################

	else_if = { limit = { has_country_flag = set_Neutral_Libertarian }
		if = { limit = { check_variable = { Neutral_Libertarian_leader = 0 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Neutral_Libertarian_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Harry Browne"
				picture = "harry_browne.dds"
				ideology = Neutral_Libertarian
				traits = {
					neutrality_Neutral_Libertarian
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Libertarian_leader = 1 } }
			if = { limit = { date < 2005.1.2 } set_temp_variable = { b = 1 } } #skip if failed election
			if = { limit = { date < 2005.2.2 is_in_array = { ruling_party = 16 } } set_temp_variable = { b = 0 } } #Won Election/Incumbant Re-election
		}
		if = { limit = { check_variable = { Neutral_Libertarian_leader = 1 }  NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Neutral_Libertarian_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Michael Badnarik"
				picture = "michael_badnarik.dds"
				ideology = Neutral_Libertarian
				traits = {
					neutrality_Neutral_Libertarian
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Libertarian_leader = 1 } }
			if = { limit = { date < 2009.1.2 } set_temp_variable = { b = 1 } } #skip if failed election
			if = { limit = { date < 2009.2.2 is_in_array = { ruling_party = 16 } } set_temp_variable = { b = 0 } } #Won Election/Incumbant Re-election
		}
		if = { limit = { check_variable = { Neutral_Libertarian_leader = 2 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Neutral_Libertarian_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Bob Barr"
				picture = "bob_barr.dds"
				ideology = Neutral_Libertarian
				traits = {
					neutrality_Neutral_Libertarian
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Libertarian_leader = 1 } }
			if = { limit = { date < 2013.1.2 } set_temp_variable = { b = 1 } } #skip if failed election
			if = { limit = { date < 2013.2.2 is_in_array = { ruling_party = 16 } } set_temp_variable = { b = 0 } } #Won Election/Incumbant Re-election
		}
		if = { limit = { check_variable = { Neutral_Libertarian_leader = 3 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Neutral_Libertarian_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Gary Johnson"
				picture = "Gary_Johnson.dds"
				ideology = Neutral_Libertarian
				traits = {
					neutrality_Neutral_Libertarian
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Libertarian_leader = 1 } }
			if = { limit = { date < 2017.1.2 } set_temp_variable = { b = 1 } } #skip if failed election
			if = { limit = { date < 2017.2.2 is_in_array = { ruling_party = 16 } } set_temp_variable = { b = 0 } } #Won Election/Incumbant Re-election
		}
		if = { limit = { check_variable = { Neutral_Libertarian_leader = 4 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Neutral_Libertarian_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Jo Jorgensen"
				picture = "jo_jorgensen.dds"
				ideology = Neutral_Libertarian
				traits = {
					neutrality_Neutral_Libertarian
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Libertarian_leader = 1 } }
			if = { limit = { date < 2021.1.2 } set_temp_variable = { b = 1 } } #skip if failed election
			if = { limit = { date < 2021.2.2 is_in_array = { ruling_party = 16 } } set_temp_variable = { b = 0 } } #Won Election/Incumbant Re-election
		}
	}

	############ Green Party Leaders ####################

	else_if = { limit = { has_country_flag = set_Neutral_green }
		if = { limit = { check_variable = { Neutral_green_leader = 0 } }
			add_to_variable = { Neutral_green_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Green National Committee"
				picture = "Green_National_Committee.dds"
				ideology = Neutral_green
				traits = {
					neutrality_Neutral_green
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_green_leader = 1 } }
		}
	}

	############ National Populist Leaders ####################

	else_if = { limit = { has_country_flag = set_Nat_Populism }
		if = { limit = { check_variable = { Nat_Populism_leader = 0 } }
			add_to_variable = { Nat_Populism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Richard Armey"
				picture = "dick_armey.dds"
				ideology = Nat_Populism
				traits = {
					nationalist_Nat_Populism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Populism_leader = 1 } }
		}
		if = { limit = { check_variable = { Nat_Populism_leader = 1 } }
			add_to_variable = { Nat_Populism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Ann Coulter"
				picture = "Ann_Coulter.dds"
				ideology = Nat_Populism
				traits = {
					nationalist_Nat_Populism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Populism_leader = 1 } }
		}
		if = { limit = { check_variable = { Nat_Populism_leader = 2 } }
			add_to_variable = { Nat_Populism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Katrina Pierson"
				picture = "katrina_pierson.dds"
				ideology = Nat_Populism
				traits = {
					nationalist_Nat_Populism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Populism_leader = 1 } }
		}
	}

	############ Fascist Leaders ####################

	else_if = { limit = { has_country_flag = set_Nat_Fascism }
		if = { limit = { check_variable = { Nat_Fascism_leader = 0 } }
			add_to_variable = { Nat_Fascism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Matt Koehl"
				picture = "Matthias_Koehl.dds"
				ideology = Nat_Fascism
				traits = {
					nationalist_Nat_Fascism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Fascism_leader = 1 } }
		}
		if = { limit = { check_variable = { Nat_Fascism_leader = 1 } }
			add_to_variable = { Nat_Fascism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Rocky Suhayda"
				picture = "Rocky_Suhayda.dds"
				ideology = Nat_Fascism
				traits = {
					nationalist_Nat_Fascism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Fascism_leader = 1 } }
		}
		if = { limit = { check_variable = { Nat_Fascism_leader = 2 } }
			add_to_variable = { Nat_Fascism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "David Duke"
				picture = "david_duke.dds"
				ideology = Nat_Fascism
				traits = {
					nationalist_Nat_Fascism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Fascism_leader = 1 } }
		}
	}

	############ Military Junta Leaders ####################

	else_if = { limit = { has_country_flag = set_Nat_Autocracy }
		if = { limit = { check_variable = { Nat_Autocracy_leader = 0 } }
			add_to_variable = { Nat_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "The Joint Chiefs of Staff"
				picture = "Joint_Chiefs_of_Staff.dds"
				ideology = Nat_Autocracy
				traits = {
					nationalist_Nat_Autocracy
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Autocracy_leader = 1 } }
		}
	}
}