ideas = {
	tank_manufacturer = {
		designer = yes
		ROM_romarm_tank_manufacturer = {
			allowed = { original_tag = ROM }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ROM_romarm_tank_manufacturer" }
			picture = ROMARM
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_armor = 0.155
			}

			traits = { CAT_armor_5 }
			ai_will_do = {
				factor = 1
			}
		}
		ROM_mfa_mizil_tank_manufacturer = {
			allowed = { original_tag = ROM }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ROM_mfa_mizil_tank_manufacturer" }
			picture = MFA_Mizil
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_afv = 0.155
			}

			traits = { CAT_afv_5 }
			ai_will_do = {
				factor = 1
			}
		}

		ROM_iar_tank_manufacturer = {
			allowed = { original_tag = ROM }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ROM_iar_tank_manufacturer" }
			picture = IAR
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_heli = 0.186
			}

			traits = { CAT_heli_6 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	materiel_manufacturer = {
		designer = yes
		ROM_ratmil_materiel_manufacturer = {
			allowed = { original_tag = ROM }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ROM_ratmil_materiel_manufacturer" }
			picture = ROMARM # TODO: Replace with the Company Logo
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_inf_wep = 0.155
			}

			traits = { CAT_inf_wep_5 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	aircraft_manufacturer = {

		designer = yes

		ROM_avioane_craiova_aircraft_manufacturer = {
			allowed = { original_tag = ROM }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ROM_avioane_craiova_aircraft_manufacturer" }
			picture = Avioane_Craiova
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_fighter = 0.155
			}

			traits = { CAT_fighter_5 }
			ai_will_do = {
				factor = 1
			}
		}
		ROM_aerostar_aircraft_manufacturer = {
			allowed = { original_tag = ROM }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ROM_aerostar_aircraft_manufacturer" }

			picture = Aerostar

			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_fixed_wing = 0.155
			}

			traits = {
				CAT_fixed_wing_5

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

	naval_manufacturer = {
		designer = yes
		ROM_galati_shipyard_naval_manufacturer = {
			allowed = { original_tag = ROM }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ROM_galati_shipyard_naval_manufacturer" }
			picture = Damen_Galati
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_surface_ship = 0.217
			}

			traits = { CAT_surface_ship_6 }
			ai_will_do = {
				factor = 1
			}
		}
	}
}
