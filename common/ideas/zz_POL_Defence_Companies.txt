ideas = {
	materiel_manufacturer = {
		designer = yes
		POL_polska_grupa_zbrojeniowa_materiel_manufacturer = {
			allowed = { original_tag = POL }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea POL_polska_grupa_zbrojeniowa_materiel_manufacturer" }
			picture = PZG
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_inf = 0.186
			}

			traits = { CAT_inf_6 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	tank_manufacturer = {
		designer = yes
		POL_polska_grupa_zbrojeniowa_tank_manufacturer = {
			allowed = { original_tag = POL }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea POL_polska_grupa_zbrojeniowa_tank_manufacturer" }
			picture = PZG
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_artillery = 0.186
			}

			traits = { CAT_artillery_6 }
			ai_will_do = {
				factor = 1
			}
		}
		POL_obrum_tank_manufacturer = {
			allowed = { original_tag = POL }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea POL_obrum_tank_manufacturer" }
			picture = OBRUM
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_armor = 0.248
			}

			traits = { CAT_armor_8 }
			ai_will_do = {
				factor = 1
			}
		}
		POL_bumar_tank_manufacturer = {
			allowed = { original_tag = POL }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea POL_bumar_tank_manufacturer" }
			picture = Bumar
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_tanks = 0.217
			}

			traits = { CAT_tanks_7 }
			ai_will_do = {
				factor = 1
			}
		}
		POL_wzm_tank_manufacturer = {
			allowed = { original_tag = POL }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea POL_wzm_tank_manufacturer" }
			picture = WZM
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_afv = 0.155
			}

			traits = { CAT_afv_5 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	naval_manufacturer = {
		designer = yes
		POL_gdansk_shipyard_naval_manufacturer = {
			allowed = { original_tag = POL }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea POL_gdansk_shipyard_naval_manufacturer" }
			picture = Gdansk_Shipyard
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_naval_eqp = 0.093
			}

			traits = { CAT_naval_eqp_3 }
			ai_will_do = {
				factor = 1
			}
		}
	}
}
