ideas = {
	country = {
		the_rubber_baron_of_africa = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea the_rubber_baron_of_africa" }
			allowed = { always = yes }
			allowed_civil_war = { always = yes }
			picture = rubber_baron_icon

			modifier = {
				political_power_factor = 0.05
				rubber_export_multiplier_modifier = 0.25
				gdp_from_resource_sector_modifier = 0.10
			}
		}
	}
}
