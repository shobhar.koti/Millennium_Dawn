ideas = {

	materiel_manufacturer = {

		designer = yes

		COL_indumil_materiel_manufacturer = {
			allowed = { original_tag = COL }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea COL_indumil_materiel_manufacturer" }

			picture = Indumil
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_inf_wep = 0.124
			}

			traits = {
				CAT_inf_wep_4

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

}
