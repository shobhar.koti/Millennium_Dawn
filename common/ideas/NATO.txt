ideas = {
	country = {
	NATO_idea_Nuclear_sharing_Groupe = {
			picture = air_support
			allowed = {
 				has_government = democratic
				has_idea = NATO_member
			}
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea GER_idea_Nuclear_sharing_Groupe" }
			allowed_civil_war = {
				has_government = democratic
			}
			modifier = {
				political_power_factor = -0.02
			}
		}
	}
}
