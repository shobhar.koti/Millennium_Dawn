ideas = {
	materiel_manufacturer = {
		designer = yes
		KOR_daewoo_firearms_materiel_manufacturer = {
			allowed = { original_tag = KOR }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea KOR_daewoo_firearms_materiel_manufacturer" }
			picture = Daewoo_logo
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_inf_wep = 0.279
			}

			traits = { CAT_inf_wep_9 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	tank_manufacturer = {
		designer = yes
		KOR_hanwha_defense_systems_tank_manufacturer = {
			allowed = { original_tag = KOR }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea KOR_hanwha_defense_systems_tank_manufacturer" }
			picture = Hanwha_defence_systems
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_afv = 0.248
			}

			traits = { CAT_afv_8 }
			ai_will_do = {
				factor = 1
			}
		}
		KOR_hyundai_rotem_tank_manufacturer = {
			allowed = { original_tag = KOR }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea KOR_hyundai_rotem_tank_manufacturer" }
			picture = Hyundai_Rotem
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_tanks = 0.248
			}

			traits = { CAT_tanks_8 }
			ai_will_do = {
				factor = 1
			}
		}
		KOR_hanwha_techwin_tank_manufacturer = {
			allowed = { original_tag = KOR }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea KOR_hanwha_techwin_tank_manufacturer" }
			picture = Hanwha_techwin
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_artillery = 0.248
			}

			traits = { CAT_artillery_8 }
			ai_will_do = {
				factor = 1
			}
		}
		KOR_korea_aerospace_industries_tank_manufacturer = {
			allowed = { original_tag = KOR }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea KOR_korea_aerospace_industries_tank_manufacturer" }
			picture = Korea_Aerospace_Industries
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_heli = 0.248
			}

			traits = { CAT_heli_8 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	aircraft_manufacturer = {
		designer = yes
		KOR_korea_aerospace_industries_aircraft_manufacturer = {
			allowed = { original_tag = KOR }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea KOR_korea_aerospace_industries_aircraft_manufacturer" }
			picture = Korea_Aerospace_Industries
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_l_fighter = 0.248
			}

			traits = { CAT_l_fighter_8 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	naval_manufacturer = {
		designer = yes
		KOR_hanjin_heavy_industries_naval_manufacturer = {
			allowed = { original_tag = KOR }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea KOR_hanjin_heavy_industries_naval_manufacturer" }
			picture = Hanjin_Heavy_Industries
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_carrier = 0.248
			}

			traits = { CAT_carrier_8 }
			ai_will_do = {
				factor = 1
			}
		}

		KOR_hyundai_heavy_industries_naval_manufacturer = {
			allowed = { original_tag = KOR  }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea KOR_hyundai_heavy_industries_naval_manufacturer" }
			picture = Hyundai_Heavy_Industries
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_surface_ship = 0.217
			}

			traits = { CAT_surface_ship_7 }
			ai_will_do = {
				factor = 1
			}
		}

		KOR_daewoo_shipbuilding_naval_manufacturer = {
			allowed = { original_tag = KOR }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea KOR_daewoo_shipbuilding_naval_manufacturer" }
			picture = Daewoo_Shipbuilding
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_surface_ship = 0.248
			}

			traits = { CAT_naval_eqp_8 }
			ai_will_do = {
				factor = 1
			}
		}

		KOR_hyundai_heavy_industries_naval_manufacturer2 = {
			allowed = { original_tag = KOR }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea KOR_hyundai_heavy_industries_naval_manufacturer" }
			picture = Hyundai_Heavy_Industries
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_d_sub = 0.217
			}

			traits = { CAT_d_sub_7 }
			ai_will_do = {
				factor = 1
			}
		}

		KOR_daewoo_shipbuilding_naval_manufacturer2 = {
			allowed = { original_tag = KOR }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea KOR_daewoo_shipbuilding_naval_manufacturer" }
			picture = Daewoo_Shipbuilding
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_d_sub = 0.248
			}

			traits = { CAT_d_sub_8 }
			ai_will_do = {
				factor = 1
			}
		}
	}
}
