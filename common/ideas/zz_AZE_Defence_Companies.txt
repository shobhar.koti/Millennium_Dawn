ideas = {
	materiel_manufacturer = {
		designer = yes
		AZE_azerbaijani_defense_industry_materiel_manufacturer = {
			allowed = { original_tag = AZE }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea AZE_azerbaijani_defense_industry_materiel_manufacturer" }
			picture = Azerbaijani_Defense_Industry_AZE
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_inf = 0.124
			}

			traits = {
				CAT_inf_4

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

	tank_manufacturer = {
		designer = yes
		AZE_azerbaijani_defense_industry_tank_manufacturer = {
			allowed = { original_tag = AZE }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea AZE_azerbaijani_defense_industry_tank_manufacturer" }
			picture = Azerbaijani_Defense_Industry_AZE
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_afv = 0.124
			}

			traits = {
				CAT_afv_4
			}
			ai_will_do = {
				factor = 1
			}
		}
	}
}