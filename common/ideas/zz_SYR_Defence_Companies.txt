ideas = {

	tank_manufacturer = {

		designer = yes

		SYR_hmisho_vehicles = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SYR_hmisho_vehicles" }

			picture = Hmisho_Vehicles

			allowed = {
				OR = {
					original_tag = SYR
					original_tag = FSA
					original_tag = NUS
				}
			}

			available = {
				has_completed_focus = SYR_subsidise_hmisho_vehicles
			}

			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_util = 0.10
			}

			traits = {
				CAT_afv_1
			}
			ai_will_do = {
				factor = 1
			}
		}
	}

	materiel_manufacturer = {

		designer = yes

		SYR_damascus_steel = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SYR_damascus_steel" }

			picture = Damascus_Steel

			allowed = {
				OR = {
					original_tag = SYR
					original_tag = FSA
					original_tag = NUS
				}
			}

			available = {
				has_completed_focus = SYR_form_damascus_steel
			}

			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_inf_wep = 0.05
			}

			traits = {
				CAT_inf_wep_2

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

}
