#scripted triggers for Division Medals feature

MD_should_have_german_medals_trigger = {
	original_tag = GER
}

MD_should_have_usa_medals_trigger = {
	original_tag = USA
}

MD_should_have_russian_medals_trigger = {
	AND = {
		original_tag = SOV
		NOT = { SOV = { nationalist_military_junta_are_in_power = yes } }
	}
}

MD_should_have_soviet_medals_trigger = {
	original_tag = SOV
	emerging_communist_state_are_in_power = yes
}

MD_should_have_wagner_medals_trigger = {
	OR = {
		original_tag = WAG
		SOV = { nationalist_military_junta_are_in_power = yes }
	}
}

MD_should_have_english_medals_trigger = {
	original_tag = ENG
}

MD_should_have_french_medals_trigger = {
	original_tag = FRA
}

MD_should_have_italian_medals_trigger = {
	original_tag = ITA
}

MD_should_have_japanese_medals_trigger = {
	original_tag = JAP
}

MD_should_have_chinese_medals_trigger = {
	original_tag = CHI
}

MD_should_have_iranian_medals_trigger = {
	original_tag = PER
}

MD_should_have_any_unique_medals_trigger = {
	OR = {
		MD_should_have_german_medals_trigger = yes
		MD_should_have_usa_medals_trigger = yes
		MD_should_have_russian_medals_trigger = yes
		MD_should_have_wagner_medals_trigger = yes
		MD_should_have_english_medals_trigger = yes
		MD_should_have_french_medals_trigger = yes
		MD_should_have_italian_medals_trigger = yes
		MD_should_have_japanese_medals_trigger = yes
		MD_should_have_chinese_medals_trigger = yes
		MD_should_have_iranian_medals_trigger = yes
	}
}