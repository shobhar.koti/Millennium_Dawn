sub_unit_categories = {
	category_militia		#All militia combat front line battalions (no recce etc.)
	category_non_militia		#All militia combat front line battalions (no recce etc.)

	category_light_infantry
	category_front_line
	category_all_infantry
	category_conventional_infantry
	category_support_battalions
	category_army
	category_line_artillery
	category_all_recon
	category_special_forces
	category_artillery
	category_fighter
	category_gunship
	category_cas
	category_mot_inf #new
	category_foot_inf #new
	category_AS_fighter #new
	category_Strike_fighter #new
	category_MR_fighter #new
	category_CV_fighter #new
	category_heavy_air #new
	category_nav_plane #new
	category_strat_bomber #new
	category_trans_plane #new
	category_Air_UAV #new
	category_AA_missile #new
	L_Strike_fighter #new

	category_all_armor
	category_tanks
	category_asg #new
	category_spart #new
	category_real_tanks #new
	category_apc
	category_IFV

	category_all_airborne
	category_inf_airborne #new
	category_mot_airborne #new
	category_arm_airborne #new

	category_all_amphibous
	category_inf_amphib #new
	category_mot_amphib #new
	category_arm_amphib #new

	category_air_aslt #new
	category_inf_aslt #new
	category_arm_aslt #new
	category_atk_helo #new

	category_at
	category_AA
	category_SP_AA
	category_util

	attack_helicopter
	helicopter
	category_trans_heli

	attack_submarine
	diesel_attack_submarine
	missile_submarine
}
