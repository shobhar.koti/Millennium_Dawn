add_namespace = haiti
# 2000
# Local Elections Held in May (First evidence of tampering)
country_event = {
	id = haiti.1
	title = haiti.1.t
	desc = haiti.1.d
	picture = GFX_election_day
	is_triggered_only = yes
	trigger = {
		original_tag = HAI
	}

	# Sore Losers!
	option = {
		name = haiti.1.a
		log = "[GetDateText]: [This.GetName]: haiti.1.a executed"
		country_event = { id = haiti.2 days = 5 random_days = 7 }
		ai_chance = {
			factor = 1
		}
	}

	# Recount the Votes
	option = {
		name = haiti.1.b
		log = "[GetDateText]: [This.GetName]: haiti.1.b executed"
		country_event = { id = haiti.3 days = 5 random_days = 7 }
		ai_chance = {
			factor = 0
		}
	}
}

# The Opposition Boycotts the Election
country_event = {
	id = haiti.2
	title = haiti.2.t
	desc = haiti.2.d
	picture = GFX_politics_protest
	is_triggered_only = yes
	trigger = {
		original_tag = HAI
	}

	# Absolutely Sore Losers! (Not nice, - DROID)
	option = {
		name = haiti.2.a
		log = "[GetDateText]: [This.GetName]: haiti.2.a executed"
		add_stability = -0.03
		set_party_index_to_ruling_party = yes
		set_temp_variable = { party_popularity_increase = 0.06 }
		set_temp_variable = { temp_outlook_increase = 0.06 }
		add_relative_party_popularity = yes

		set_country_flag = HAI_opposition_boycotts_the_election
		ai_chance = {
			factor = 1
		}
	}
}

# The Opposition Decides to Participate
country_event = {
	id = haiti.3
	title = haiti.3.t
	desc = haiti.3.d
	picture = GFX_politics_negotiations
	is_triggered_only = yes
	trigger = {
		original_tag = HAI
	}

	# A Fair & Free Election
	option = {
		name = haiti.3.a
		log = "[GetDateText]: [This.GetName]: haiti.3.a executed"
		set_temp_variable = { party_index = 18 }
		set_temp_variable = { party_popularity_increase = 0.06 }
		set_temp_variable = { temp_outlook_increase = 0.06 }
		add_relative_party_popularity = yes

		ai_chance = {
			factor = 1
		}
	}
}

# The 2000 Elections
country_event = {
	id = haiti.4
	title = haiti.4.t
	desc = haiti.4.d
	picture = GFX_election_day
	is_triggered_only = yes
	trigger = {
		original_tag = HAI
	}
	immediate = {
		clr_country_flag = generic_election_killswitch
	}

	# Arisitide
	option = {
		name = haiti.4.a
		log = "[GetDateText]: [This.GetName]: haiti.4.a executed"
		hidden_effect = {
			update_government_coalition_strength = yes
			update_party_name = yes
			set_coalition_drift = yes
			set_ruling_leader = yes
			set_leader = yes
		}
		set_politics = {
			ruling_party = democratic
			elections_allowed = yes
		}

		ai_chance = {
			factor = 1
		}
	}

	# Other
	option = {
		name = haiti.4.b
		log = "[GetDateText]: [This.GetName]: haiti.4.b executed"
		trigger = {
			NOT = { has_country_flag = HAI_opposition_boycotts_the_election }
		}

		hidden_effect = {
			clear_array = ruling_party
			clear_array = gov_coalition_array
			add_to_array = { ruling_party = 18 }
			update_government_coalition_strength = yes
			update_party_name = yes
			set_coalition_drift = yes
			set_ruling_leader = yes
			set_leader = yes

			add_popularity = { ideology = neutrality popularity = 0.15 }
		}
		set_politics = {
			ruling_party = neutrality
			elections_allowed = yes
		}

		ai_chance = {
			factor = 1
		}
	}
}

# 2001
# Student Protests Against Aristide!
country_event = {
	id = haiti.5
	title = haiti.5.t
	desc = haiti.5.d
	picture = GFX_haitian_student_protests
	is_triggered_only = yes
	trigger = {
		original_tag = HAI
		has_country_leader = { name = "Jean-Bertrand Aristide" ruling_only = yes }
	}

	# Negotiate with the Students
	option = {
		name = haiti.5.a
		log = "[GetDateText]: [This.GetName]: haiti.5.a executed"
		add_political_power = -50
		add_stability = 0.03

		ai_chance = {
			factor = 1
		}
	}

	# Quell the Protests
	option = {
		name = haiti.5.b
		log = "[GetDateText]: [This.GetName]: haiti.5.b executed"
		add_stability = -0.03

		ai_chance = {
			factor = 1
		}
	}
}

# 2002
# Student Protests Against Aristide!
country_event = {
	id = haiti.6
	title = haiti.6.t
	desc = haiti.6.d
	picture = GFX_haitian_student_protests
	is_triggered_only = yes
	trigger = {
		original_tag = HAI
		has_country_leader = { name = "Jean-Bertrand Aristide" ruling_only = yes }
	}

	# Negotiate with the Students
	option = {
		name = haiti.6.a
		log = "[GetDateText]: [This.GetName]: haiti.6.a executed"
		add_political_power = -50
		add_stability = 0.03

		ai_chance = {
			factor = 1
		}
	}

	# Quell the Protests
	option = {
		name = haiti.6.b
		log = "[GetDateText]: [This.GetName]: haiti.6.b executed"
		add_stability = -0.03

		ai_chance = {
			factor = 1
		}
	}
}


# 2003
# Request France pay Reparations
country_event = {
	id = haiti.7
	title = haiti.7.t
	desc = haiti.7.d
	picture = GFX_jean_bertrand_aristride
	is_triggered_only = yes
	trigger = {
		original_tag = HAI
		has_country_leader = { name = "Jean-Bertrand Aristide" ruling_only = yes }
	}

	# Pursue the Reparations
	option = {
		name = haiti.7.a
		log = "[GetDateText]: [This.GetName]: haiti.7.a executed"
		add_opinion_modifier = { target = FRA modifier = FRA_haiti_demands_reparations_opinion }
		FRA = { country_event = { id = haiti.8 days = 3 } }
		set_country_flag = HAI_demands_french_reparations
		ai_chance = {
			factor = 1
		}
	}

	# Don't Pursue the Reparations
	option = {
		name = haiti.7.b
		log = "[GetDateText]: [This.GetName]: haiti.7.b executed"
		ai_chance = {
			factor = 0
		}
	}
}

# Tropical Airways Flight 1301
country_event = {
	id = haiti.17
	title = haiti.17.t
	desc = haiti.17.d
	picture = GFX_tropical_flight_way
	is_triggered_only = yes
	trigger = {
		original_tag = HAI
	}

	# Truly a Tragedy
	option = {
		name = haiti.17.a
		log = "[GetDateText]: [This.GetName]: haiti.17.a executed"
		add_stability = -0.01
		ai_chance = {
			factor = 1
		}
	}
}

# France - Do we pay the reparations?
country_event = {
	id = haiti.8
	title = haiti.8.t
	desc = haiti.8.d
	picture = GFX_united_states_dollar
	is_triggered_only = yes
	trigger = {
		original_tag = FRA
	}

	# Pay the Reparations
	option = {
		name = haiti.8.a
		log = "[GetDateText]: [This.GetName]: haiti.8.a executed"
		effect_tooltip = {
			set_temp_variable = { treasury_change = -21 }
			modify_treasury_effect = yes
			HAI = {
				set_temp_variable = { treasury_change = 21 }
				modify_treasury_effect = yes
			}
		}

		set_country_flag = FRA_pays_the_haitian_reps
		HAI = { country_event = { id = haiti.9 days = 3 } }
		ai_chance = {
			factor = 0
		}
	}

	# Deny the Reparations
	option = {
		name = haiti.8.b
		log = "[GetDateText]: [This.GetName]: haiti.8.b executed"
		HAI = { country_event = { id = haiti.9 days = 3 } }
		ai_chance = {
			factor = 1
		}
	}
}

# Haiti - Response to the Event
country_event = {
	id = haiti.9
	title = {
		text = haiti.9.t1
		trigger = { FRA = { has_country_flag = FRA_pays_the_haitian_reps } }
	}
	title = {
		text = haiti.9.t2
		trigger = { NOT = { FRA = { has_country_flag = FRA_pays_the_haitian_reps } } }
	}
	desc = {
		text = haiti.9.d1
		trigger = { FRA = { has_country_flag = FRA_pays_the_haitian_reps } }
	}
	desc = {
		text = haiti.9.d2
		trigger = { NOT = { FRA = { has_country_flag = FRA_pays_the_haitian_reps } } }
	}
	picture = GFx_message_signing
	is_triggered_only = yes
	trigger = {
		original_tag = HAI
	}

	# France Pays the Reparations
	option = {
		name = haiti.9.a
		log = "[GetDateText]: [This.GetName]: haiti.9.a executed"
		trigger = {
			FRA = { has_country_flag = FRA_pays_the_haitian_reps }
		}

		set_temp_variable = { treasury_change = 21 }
		modify_treasury_effect = yes
		FRA = {
			set_temp_variable = { treasury_change = -21 }
			modify_treasury_effect = yes
		}
	}

	# France Denies the Reparations
	option = {
		name = haiti.9.b
		log = "[GetDateText]: [This.GetName]: haiti.9.b executed"
		trigger = {
			NOT = { FRA = { has_country_flag = FRA_pays_the_haitian_reps } }
		}
	}
}

# Amiot Métayer and the Cannibal Army
country_event = {
	id = haiti.10
	title = haiti.10.t
	desc = haiti.10.d
	picture = GFX_politics_protest # TODO: Replace with more appropriate image
	is_triggered_only = yes
	trigger = {
		original_tag = HAI
		has_country_leader = { name = "Jean-Bertrand Aristide" ruling_only = yes }
	}

	# Eliminate the Threat
	option = {
		name = haiti.10.a
		log = "[GetDateText]: [This.GetName]: haiti.10.a executed"
		country_event = { id = haiti.11 days = 8 }
		ai_chance = {
			factor = 1
		}
	}

	# Let Him Live
	option = {
		name = haiti.10.b
		log = "[GetDateText]: [This.GetName]: haiti.10.b executed"
		set_country_flag = HAI_ignores_amiot
		ai_chance = {
			factor = 0
		}
	}
}

# Amiot Metayer Found Dead
country_event = {
	id = haiti.11
	title = haiti.11.t
	desc = haiti.11.d
	picture = GFX_politics_protest # TODO: Replace with more appropriate image
	is_triggered_only = yes
	trigger = {
		original_tag = HAI
		has_country_leader = { name = "Jean-Bertrand Aristide" ruling_only = yes }
	}

	# Consequences Will Soon be Seen...
	option = {
		name = haiti.11.a
		log = "[GetDateText]: [This.GetName]: haiti.11.a executed"
		set_country_flag = HAI_amiot_killed
		add_stability = -0.03
		ai_chance = {
			factor = 1
		}
	}
}

# Aristide's Supporter Assault Université d'État d'Haïti
country_event = {
	id = haiti.12
	title = haiti.12.t
	desc = haiti.12.d
	picture = GFX_kurd_riot
	is_triggered_only = yes
	trigger = {
		original_tag = HAI
		has_country_leader = { name = "Jean-Bertrand Aristide" ruling_only = yes }
	}

	# Things are Heating Up
	option = {
		name = haiti.12.a
		log = "[GetDateText]: [This.GetName]: haiti.12.a executed"
		add_stability = -0.02
		ai_chance = {
			factor = 1
		}
	}
}

# 2004
# Student Protests Turn Violent!
country_event = {
	id = haiti.13
	title = haiti.13.t
	desc = haiti.13.d
	picture = GFX_haitian_student_protests
	is_triggered_only = yes
	trigger = {
		original_tag = HAI
		has_country_leader = { name = "Jean-Bertrand Aristide" ruling_only = yes }
	}

	# Negotiate with the Students
	option = {
		name = haiti.13.a
		log = "[GetDateText]: [This.GetName]: haiti.13.a executed"
		add_political_power = -50
		add_stability = 0.03

		ai_chance = {
			factor = 1
		}
	}

	# Quell the Protests
	option = {
		name = haiti.13.b
		log = "[GetDateText]: [This.GetName]: haiti.13.b executed"
		add_stability = -0.03

		ai_chance = {
			factor = 0
		}
	}
}

# The Cannibal Army Seizes Gonaives
country_event = {
	id = haiti.14
	title = haiti.14.t
	desc = haiti.14.d
	picture = GFX_buteur_mateyer_cannibal_army
	is_triggered_only = yes
	trigger = {
		original_tag = HAI
		has_country_flag = HAI_amiot_killed
		has_country_leader = { name = "Jean-Bertrand Aristide" ruling_only = yes }
	}

	# Tragic...
	option = {
		name = haiti.14.a
		log = "[GetDateText]: [This.GetName]: haiti.14.a executed"
		custom_effect_tooltip = haiti.14.tt

		country_event = { id = haiti.15 days = 7 }
		ai_chance = {
			factor = 1
		}
	}

	# Mobilize the Troops - Civil War Option
	option = {
		name = haiti.14.b
		log = "[GetDateText]: [This.GetName]: haiti.14.b executed"
		FRA = { country_event = { id = haiti.16 days = 7 } }
		ai_chance = {
			factor = 0
		}
	}
}

# The Cannibal Army Seizes Cap-Haitien
country_event = {
	id = haiti.15
	title = haiti.15.t
	desc = haiti.15.d
	picture = GFX_buteur_mateyer_cannibal_army
	is_triggered_only = yes
	trigger = {
		original_tag = HAI
		has_country_flag = HAI_amiot_killed
		has_country_leader = { name = "Jean-Bertrand Aristide" ruling_only = yes }
	}

	# It's time to Resign...
	option = {
		name = haiti.15.a
		log = "[GetDateText]: [This.GetName]: haiti.15.a executed"
		kill_country_leader = yes
		create_country_leader = {
			name = "Boniface Alexandre"
			picture = "boniface_alexander.dds"
			ideology = socialism
			traits = {
				western_socialism
				lawyer
				judiciary
			}
		}


		FRA = { country_event = { id = haiti.16 days = 7 } }
		ai_chance = {
			factor = 1
		}
	}

	# Mobilize the Troops! - Civil War Option
	option = {
		name = haiti.15.b
		log = "[GetDateText]: [This.GetName]: haiti.15.b executed"
		set_global_flag = HAI_haitian_civil_war
		start_civil_war = {
			ideology = democratic
			size = 0.3
			states = { 859 }
		}

		set_temp_variable = { rul_party_temp = 13 }
		change_ruling_party_effect = yes
		set_politics = {
			ruling_party = neutrality
			last_election = "2000.11.1"
			election_frequency = 60
			elections_allowed = yes
		}

		create_country_leader = {
			name = "Buteur Métayer"
			picture = "buteur_metayer.dds"
			ideology = Neutral_Autocracy
			traits = {
				neutrality_Neutral_Autocracy
			}
		}

		division_template = {
			name = "Rebel Forces"
			is_locked = yes
			regiments = {
				Militia_Bat = { x = 0 y = 0 }
				Militia_Bat = { x = 0 y = 1 }
				Militia_Bat = { x = 0 y = 2 }
				Militia_Bat = { x = 0 y = 3 }
			}
		}

		860 = {
			create_unit = {
				division = "name = \"1. Rebel Forces\" division_template = \"Rebel Forces\" start_experience_factor = 0.5 start_equipment_factor = 1"
				owner = HAI
			}
			create_unit = {
				division = "name = \"2. Rebel Forces\" division_template = \"Rebel Forces\" start_experience_factor = 0.5 start_equipment_factor = 1"
				owner = HAI
			}
		}

		random_other_country = {
			limit = {
				original_tag = HAI
				has_government = democratic
			}

			create_country_leader = {
				name = "Jean-Bertrand Aristide"
				picture = "jean-bertrand_artistide.dds"
				ideology = socialism
				traits = {
					western_socialism
					cleric
				}
			}

			division_template = {
				name = "Police Forces"
				is_locked = yes
				regiments = {
					Militia_Bat = { x = 0 y = 0 }
					Militia_Bat = { x = 0 y = 1 }
					Militia_Bat = { x = 0 y = 2 }
					Militia_Bat = { x = 0 y = 3 }
				}
			}

			859 = {
				create_unit = {
					division = "name = \"1. Police Forces\" division_template = \"Police Forces\" start_experience_factor = 0.5 start_equipment_factor = 1"
					owner = PREV
				}
				create_unit = {
					division = "name = \"2. Police Forces\" division_template = \"Police Forces\" start_experience_factor = 0.5 start_equipment_factor = 1"
					owner = PREV
				}
			}
			# Only Execute on AI
			if = {  limit = { HAI = { is_ai = no } }
				change_tag_from = HAI
			}
		}

		FRA = { country_event = { id = haiti.16 days = 7 } }
		ai_chance = {
			factor = 0
		}
	}
}


# France - The Haitian Coup has Begun!
country_event = {
	id = haiti.16
	title = haiti.16.t
	desc = haiti.16.d
	is_triggered_only = yes
	trigger = {
		original_tag = FRA
	}

	# Intervene in the Country!
	option = {
		name = haiti.16.a
		log = "[GetDateText]: [This.GetName]: haiti.16.a executed"
		HAI = {
			add_manpower = 300
		}
		add_manpower = -300
		add_relation_modifier = {
			target = HAI
			modifier = generic_increased_military_support
		}

		set_country_flag = FRA_intervening_in_haiti
		USA = { country_event = { id = haiti.18 days = 7 } }
		ai_chance = {
			factor = 1
		}
	}

	# Stay out of it
	option = {
		name = haiti.16.b
		log = "[GetDateText]: [This.GetName]: haiti.16.b executed"
		USA = { country_event = { id = haiti.18 days = 7 } }
		ai_chance = {
			factor = 0
		}
	}
}

# United States - Operation Secure Tomorrow
country_event = {
	id = haiti.18
	title = haiti.18.t
	desc = haiti.18.d
	is_triggered_only = yes
	trigger = {
		original_tag = USA
	}

	# Support the Operation
	option = {
		name = haiti.18.a
		log = "[GetDateText]: [This.GetName]: haiti.18.a executed"
		add_relation_modifier = {
			target = HAI
			modifier = generic_increased_military_support
		}

		set_country_flag = USA_intervene_in_haiti
		FRA = { country_event = { id = haiti.19 days = 5 } }
		ai_chance = {
			factor = 1
		}
	}

	# Denounce the Operation
	option = {
		name = haiti.18.b
		log = "[GetDateText]: [This.GetName]: haiti.18.b executed"
		FRA = { country_event = { id = haiti.19 days = 5 } }
		ai_chance = {
			factor = 0
		}
	}
}

# France - US Supports the Operation
country_event = {
	id = haiti.19
	title = {
		text = haiti.19.t1
		trigger = { USA = { has_country_flag = USA_intervene_in_haiti } }
	}
	title = {
		text = haiti.19.t2
		trigger = { NOT = { USA = { has_country_flag = USA_intervene_in_haiti } } }
	}
	desc = {
		text = haiti.19.d1
		trigger = { USA = { has_country_flag = USA_intervene_in_haiti } }
	}
	desc = {
		text = haiti.19.d2
		trigger = { NOT = { USA = { has_country_flag = USA_intervene_in_haiti } } }
	}
	is_triggered_only = yes
	trigger = {
		original_tag = FRA
	}

	# America Supports France
	option = {
		name = haiti.19.a
		log = "[GetDateText]: [This.GetName]: haiti.19.a executed"
		trigger = {
			USA = { has_country_flag = USA_intervene_in_haiti }
			has_country_flag = FRA_intervening_in_haiti
		}

		HAI = { country_event = { id = haiti.20 days = 7 } }
		ai_chance = {
			factor = 1
		}
	}

	# America Doesn't Supports Us
	option = {
		name = haiti.19.b
		log = "[GetDateText]: [This.GetName]: haiti.19.b executed"
		trigger = {
			NOT = { USA = { has_country_flag = USA_intervene_in_haiti } }
			has_country_flag = FRA_intervening_in_haiti
		}

		HAI = { country_event = { id = haiti.20 days = 7 } }
		ai_chance = {
			factor = 1
		}
	}

	# We Really Should Avoid Getting Involved
	option = {
		name = haiti.19.c
		log = "[GetDateText]: [This.GetName]: haiti.19.c executed"
		trigger = {
			USA = { has_country_flag = USA_intervene_in_haiti }
			NOT = { has_country_flag = FRA_intervening_in_haiti }
		}

		HAI = { country_event = { id = haiti.20 days = 7 } }
		ai_chance = {
			factor = 1
		}
	}
}

# Haiti - Deployment of French/American Forces
country_event = {
	id = haiti.20
	title = haiti.20.t
	desc = haiti.20.d
	picture = GFX_american_troops
	is_triggered_only = yes
	trigger = {
		original_tag = HAI
	}

	option = {
		name = haiti.20.a
		log = "[GetDateText]: [This.GetName]: haiti.20.a executed"
		trigger = {
			OR = {
				USA = { has_country_flag = USA_intervene_in_haiti }
				FRA = { has_country_flag = FRA_intervening_in_haiti }
			}
		}
		if = { limit = { FRA = { has_country_flag = FRA_intervening_in_haiti } }
			custom_effect_tooltip = haiti.20.tt1
		}
		if = { limit = { USA = { has_country_flag = USA_intervene_in_haiti } }
			custom_effect_tooltip = haiti.20.tt2
		}
		add_war_support = 0.05
		ai_chance = {
			factor = 1
		}
	}

	option = {
		name = haiti.20.b
		log = "[GetDateText]: [This.GetName]: haiti.20.b executed"
		trigger = {
			NOT = { USA = { has_country_flag = USA_intervene_in_haiti } }
			NOT = { FRA = { has_country_flag = FRA_intervening_in_haiti } }
		}
		add_war_support = -0.05

		ai_chance = {
			factor = 1
		}
	}
}

# Haiti - The Civil War is Over!
country_event = {
	id = haiti.21
	title = haiti.21.t
	desc = {
		text = haiti.21.d1
		trigger = {
			has_country_leader = { name = "Jean-Bertrand Aristide" ruling_only = yes }
		}
	}
	desc = {
		text = haiti.21.d2
		trigger = {
			has_country_leader = { name = "Buteur Métayer" ruling_only = yes }
		}
	}
	picture = GFX_jean_bertrand_aristide_resigns
	is_triggered_only = yes
	trigger = {
		original_tag = HAI
		has_global_flag = HAI_haitian_civil_war
		NOT = { has_global_flag = HAI_haitian_civil_war_cleanedup }
	}

	# A New Dawn
	option = {
		name = haiti.21.a
		log = "[GetDateText]: [This.GetName]: haiti.21.a executed"
		add_stability = 0.05
		set_party_index_to_ruling_party = yes
		set_temp_variable = { party_popularity_increase = 0.10 }
		set_temp_variable = { temp_outlook_increase = 0.10 }
		add_relative_party_popularity = yes

		hidden_effect = {
			if = { limit = { tag = HAI }
				delete_unit_template_and_units = {
					division_template = "Rebel Forces"
				}
			}
			else = {
				delete_unit_template_and_units = {
					division_template = "Police Forces"
				}
			}
			if = {
				limit = {
					FRA = {
						has_relation_modifier = {
							target = HAI
							modifier = generic_increased_military_support
						}
					}
				}
				FRA = {
					remove_relation_modifier = {
						target = HAI
						modifier = generic_increased_military_support
					}
				}
			}
			if = {
				limit = {
					USA = {
						has_relation_modifier = {
							target = HAI
							modifier = generic_increased_military_support
						}
					}
				}
				USA = {
					remove_relation_modifier = {
						target = HAI
						modifier = generic_increased_military_support
					}
				}
			}
		}
		if = {
			limit = { has_country_leader = { name = "Jean-Bertrand Aristide" ruling_only = yes } }
			# Resets the Tag to HAI
			THIS = {
				change_tag_from = HAI
			}
		}
		else = {
			set_temp_variable = { rul_party_temp = 3 }
			change_ruling_party_effect = yes
			set_politics = {
				ruling_party = democratic
				last_election = "2000.11.1"
				election_frequency = 60
				elections_allowed = yes
			}
			create_country_leader = {
				name = "Boniface Alexandre"
				picture = "boniface_alexander.dds"
				ideology = socialism
				traits = {
					western_socialism
					lawyer
					judiciary
				}
			}
		}
		set_cosmetic_tag = HAI
		set_global_flag = HAI_haitian_civil_war_cleanedup
	}
}
