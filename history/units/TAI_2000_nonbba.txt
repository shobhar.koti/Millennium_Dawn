﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = transport_plane1 #USA_transport_plane1:0 "C-130 Hercules"
		amount = 24
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Boeing CH-47 Chinook
		amount = 8
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = Strike_fighter2 #Mirage 2000 DI/EI
		amount = 58
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = MR_Fighter1 #"F-5E Tiger II & F5-F" 265
		amount = 265
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = MR_Fighter2 #F-CK-1 Ching-kuo
		amount = 128
	}
	add_equipment_to_stockpile = {
		type = L_Strike_fighter2 #AT-3
		amount = 58
	}
	add_equipment_to_stockpile = {
		type = MR_Fighter2 #F-16A/B Fighting Falcon
		amount = 126
		producer = USA
	}
}