instant_effect = {

	add_equipment_to_stockpile = {
		type = L_Strike_fighter1
		#variant_name = "Aero L-39"
		amount = 14
		producer = CZE
	}

	add_equipment_to_stockpile = {
		type = L_Strike_fighter1
		#variant_name = "Aermacchi MB-339"
		amount = 2
		producer = ITA
	}

	add_equipment_to_stockpile = {
		type = L_Strike_fighter1
		#variant_name = "Aermacchi MB-326"
		amount = 3
		producer = ITA
	}

	add_equipment_to_stockpile = {
		type = transport_plane1
		#variant_name = "Fokker F27 Friendship"
		amount = 5
		producer = HOL
	}

	add_equipment_to_stockpile = {
		type = transport_plane1
		#variant_name = "C-212 Aviocar"
		amount = 1
		producer = SPR
	}

	add_equipment_to_stockpile = {
		type = transport_plane1
		#variant_name = "Short SC.7 Skyvan"
		amount = 6
		producer = ENG
	}

	add_equipment_to_stockpile = {
		type = transport_plane1
		#variant_name = "C-20 Gulfstream"
		amount = 1
		producer = USA
	}

}