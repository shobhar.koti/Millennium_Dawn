division_template = {
	name = "Battaglione Di Polizia"

	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
	}
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = infantry_weapons1
		amount = 150
		producer = SMA
	}
	add_equipment_to_stockpile = {
		type = infantry_weapons
		amount = 300
		producer = SMA
	}
	add_equipment_to_stockpile = {
		type = command_control_equipment1
		amount = 45
		producer = SMA
	}
}