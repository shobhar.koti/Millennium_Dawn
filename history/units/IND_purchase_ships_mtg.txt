units = {
	fleet = {
		name = "Western Fleet"
		naval_base = 4279
		task_force = {
			name = "Western Fleet 2"
			location = 4279
			ship = { name = "Spejik Frigate" definition = frigate start_experience_factor = 0.30 equipment = { frigate_hull_1 = { amount = 1 owner = IND creator = HOL version_name = "Spejik Class" } } }
		}
	}
}