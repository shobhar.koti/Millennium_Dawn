﻿division_template = {
	name = "Bataillon d’Infanterie"

	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
	}

	support = {
		armor_Recce_Comp = { x = 0 y = 0 }
	}
}

units = {
	division = {
		name = "1er Bataillon d’Infanterie"
		location = 10803	#Abidjan
		division_template = "Bataillon d’Infanterie"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	division = {
		name = "2éme Bataillon d’Infanterie"
		location = 9372	#Daloa
		division_template = "Bataillon d’Infanterie"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	division = {
		name = "3éme Bataillon d’Infanterie"
		location = 5070	#
		division_template = "Bataillon d’Infanterie"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = infantry_weapons
		amount = 1200
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = command_control_equipment
		amount = 150
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Anti_tank_0
		amount = 50
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_0
		amount = 50
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Anti_Air_0
		amount = 50
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_0
		amount = 5
		producer = FRA
		variant_name = "AMX 13"
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_1
		amount = 7
		producer = FRA
		variant_name = "Panhard ERC 90"
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_0
		amount = 16
		producer = FRA
		variant_name = "Panhard AML 90"
	}
}