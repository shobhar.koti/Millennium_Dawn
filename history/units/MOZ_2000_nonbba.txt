instant_effect = {
	
	add_equipment_to_stockpile = {
		type = transport_plane1
		#variant_name = "An-26"
		amount = 5
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = transport_plane1
		#variant_name = "C-212 Aviocar"
		amount = 2
		producer = SPR
	}
}