﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = MR_Fighter1 #"F-5 Freedom Fighter"
		amount = 15
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = L_Strike_fighter1 #Embraer EMB-326k
		amount = 3
		producer = BRA
	}
	add_equipment_to_stockpile = {
		type = L_Strike_fighter1 #Embraer EMB-326L
		amount = 2
		producer = BRA
	}
	add_equipment_to_stockpile = {
		type = transport_plane1 #C-130B
		amount = 5
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_plane1 #C-130H
		amount = 2
		producer = USA
	}
}