﻿division_template = {
	name = "Infantry Brigade"

	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 0 y = 1 }
		L_Inf_Bat = { x = 0 y = 2 }
		L_Inf_Bat = { x = 0 y = 3 }
		L_Inf_Bat = { x = 0 y = 4 }
		Mech_Inf_Bat = { x = 1 y = 0 }
		arty_bat = { x = 2 y = 0 }
		arty_bat = { x = 2 y = 1 }
	}
	support = {
		Mech_Recce_Comp = { x = 0 y = 0 }
	}
}

division_template = {
	name = "Special Forces"

	regiments = {
		Special_Forces = { x = 0 y = 0 }
	}
}

units = {
	division = {
		name = "1st New Zealand Brigade"
		location = 1814
		division_template = "Infantry Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		name = "1st NZSAS Regiment"
		location = 4543
		division_template = "Special Forces"
		start_experience_factor = 0.9
		start_equipment_factor = 0.01
	}
	division = {
		name = "2nd NZSAS Regiment"
		location = 4543
		division_template = "Special Forces"
		start_experience_factor = 0.9
		start_equipment_factor = 0.01
	}
}

instant_effect = {
	# VECHICLES ###
	add_equipment_to_stockpile = {
		type = light_tank_hull_1
		amount = 8
		producer = ENG
		variant_name = "FV101 Scorpion"
	}

	add_equipment_to_stockpile = {
		type = apc_hull_1
		variant_name = "M113"
		amount = 77
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = artillery_1 # "ART 1965" #hamel
		amount = 24
		producer = ENG
	}

	add_equipment_to_stockpile = {
		type = Anti_tank_2 # "MBT LAW"
		amount = 50
		producer = ENG
	}

	add_equipment_to_stockpile = {
		type = Anti_tank_0 #"Carl Gustaf"
		amount = 63
		producer = SWE
	}

	add_equipment_to_stockpile = {
		type = Anti_Air_1 #Mistral
		amount = 70
		producer = FRA
	}

	###INFANTRY EQUIPMENT####
	add_equipment_to_stockpile = {
		type = infantry_weapons1 #Steyr AUG
		amount = 2200
		producer = AUS
	}
	add_equipment_to_stockpile = {
		type = command_control_equipment2 #C2ISTAR Equipment
		amount = 150
	}
}