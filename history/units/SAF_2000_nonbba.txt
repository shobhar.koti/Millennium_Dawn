﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = MR_Fighter1 #Cheetah
		amount = 39
		producer = SAF
	}
	add_equipment_to_stockpile = {
		type = L_Strike_fighter1 #MB-326 (Impala)
		amount = 48
		producer = ITA
	}
	add_equipment_to_stockpile = {
		type = transport_plane1 #C-130 Hercules
		amount = 12
		producer = USA
	}
}