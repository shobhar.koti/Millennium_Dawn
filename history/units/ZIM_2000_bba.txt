instant_effect = {

	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "BAE Hawk T1"
		amount = 8
		producer = ENG
	}

	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "Hawk 200"
		amount = 11
		producer = ENG
	}

	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "F-7 Airguard"
		amount = 12
		producer = CHI
	}

	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "SIAI-Marchetti SF.260"
		amount = 22
		producer = ITA
	}

	add_equipment_to_stockpile = {
		type = medium_plane_maritime_patrol_airframe_1
		variant_name = "BN-2 Defender"
		amount = 6
		producer = ENG
	}

	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "C-212 Aviocar"
		amount = 8
		producer = SPR
	}

}