instant_effect = {

	add_equipment_to_stockpile = {
		type = L_Strike_fighter1
		#variant_name = "Pilatus PC-7"				#2x additional units in place of PC-6
		amount = 4
		producer = SWI
	}

	add_equipment_to_stockpile = {
		type = L_Strike_fighter1
		#variant_name = "SIAI-Marchetti SF.260"
		amount = 2
		producer = ITA
	}

	add_equipment_to_stockpile = {
		type = transport_plane2
		#variant_name = "C-130 Hercules"
		amount = 3
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = transport_plane2
		#variant_name = "C-212 Aviocar"
		amount = 1
		producer = SPR
	}

	add_equipment_to_stockpile = {
		type = transport_plane2
		#variant_name = "An-26"
		amount = 1
		producer = SOV
	}

}

	