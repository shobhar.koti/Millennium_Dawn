units = {
	fleet = {
		name = "Western Fleet"
		naval_base = 4279
		task_force = {
			name = "Western Fleet 2"
			location = 4279
			ship = { name = "Sigma Corvette" definition = corvette start_experience_factor = 0.30 equipment = { corvette_hull_2 = { amount = 1 owner = IND creator = SOV version_name = "Sigma Class" } } }
		}
	}
}