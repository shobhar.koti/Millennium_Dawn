version="1.10"
tags={
	"Alternative History"
	"Events"
	"National Focuses"
	"Translation"
	"Fixes"
	"Utilities"
}
dependencies={
	"Millennium Dawn: A Modern Day Mod"
}
name="Millennium Dawn: Taiwan"
supported_version="1.14.5"
