set_leader_KEN = {

	if = { limit = { has_country_flag = set_liberalism }

		if = { limit = { check_variable = { liberalism_leader = 0 } }
			add_to_variable = { liberalism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Raila Odinga"
				picture = "Raila_Odinga.dds"
				desc = "POLITICS_RAILA_ODINGA_DESC"
				ideology = liberalism
				traits = {
					ethnic_luo
					career_politician
					western_liberalism
					sly
					tribalist
					anti_kenyatta
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { liberalism_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
	}
	else_if = { limit = { has_country_flag = set_socialism }
		if = { limit = { check_variable = { socialism_leader = 0 } }
			add_to_variable = { socialism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Simeon Nyachae"
				picture = "Simeon_Nyachae.dds"
				ideology = socialism
				traits = {
					ethnic_kisii
					career_politician
					western_socialism
					tribalist
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { socialism_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
	}
	else_if = { limit = { has_country_flag = set_Communist-State }
		if = { limit = { check_variable = { Communist-State_leader = 0 } }
			add_to_variable = { Communist-State_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "James Orengo"
				picture = "James_Orengo.dds"
				ideology = Communist-State
				traits = {
					ethnic_luo
					lawyer
					emerging_Communist-State
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Communist-State_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
	}
	else_if = { limit = { has_country_flag = set_anarchist_communism }
		if = { limit = { check_variable = { anarchist_communism_leader = 0 } }
			add_to_variable = { anarchist_communism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "George Anyona"
				picture = "George_Anyona.dds"
				ideology = anarchist_communism
				traits = {
					ethnic_kisii
					farmer
					emerging_anarchist_communism
					ruthless
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { anarchist_communism_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
		if = { limit = { check_variable = { anarchist_communism_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { anarchist_communism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Peter Kenneth"
				picture = "Peter_Kenneth.dds"
				ideology = anarchist_communism
				traits = {
					ethnic_kikuyu
					lawyer
					emerging_anarchist_communism
					humble
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { anarchist_communism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Neutral_Autocracy }
	if = { limit = { check_variable = { Neutral_Autocracy_leader = 0 } }
		add_to_variable = { Neutral_Autocracy_leader = 1 }
		hidden_effect = { kill_country_leader = yes }

		create_country_leader = {
			name = "Uhuru Kenyatta"
			picture = "Uhuru_Kenyatta.dds"
			desc = "POLITICS_UHURU_KENYATTA_2002_DESC"
			ideology = Neutral_Autocracy
			traits = {
				ethnic_kikuyu
				businessman
				neutrality_Neutral_Autocracy
				inexperienced
				moi_puppet
				tribalist
				anti_odinga
			}
		}

		if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Autocracy_leader = 1 } }
		if = { limit = { date < 2008.1.1 } set_temp_variable = { b = 1 } } #skip if 2008
	}
	if = { limit = { check_variable = { Neutral_Autocracy_leader = 1 } NOT = { check_variable = { b = 1 } } }
		add_to_variable = { Neutral_Autocracy_leader = 1 }
		hidden_effect = { kill_country_leader = yes }

		create_country_leader = {
			name = "Gideon Moi"
			picture = "Gideon_Moi.dds"
			desc = "POLITICS_GIDEON_MOI_DESC"
			ideology = Neutral_Autocracy
			traits = {
				ethnic_kalenjin
				career_politician
				neutrality_Neutral_Autocracy
				capable
				moi_puppet
				tribalist
			}
		}

		if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Autocracy_leader = 1 } }
		set_temp_variable = { b = 1 }
	}
}
	else_if = { limit = { has_country_flag = set_Neutral_conservatism }
		if = { limit = { check_variable = { Neutral_conservatism_leader = 0 } }
			add_to_variable = { Neutral_conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Mwai Kibaki"
				picture = "Mwai_Kibaki.dds"
				desc = "POLITICS_MWAI_KIBAKI_DESC"
				ideology = Neutral_conservatism
				traits = {
					ethnic_kikuyu
					businessman
					neutrality_Neutral_conservatism
					likeable
					capable
					humble
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_conservatism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Neutral_Libertarian }
		if = { limit = { check_variable = { Neutral_Libertarian_leader = 0 } }
			add_to_variable = { Neutral_Libertarian_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Richard Leakey"
				picture = "Richard_Leakey.dds"
				desc = "POLITICS_RICHARD_LEAKEY_DESC"
				ideology = Neutral_Libertarian
				traits = {
					ethnic_others
					archaeologist
					neutrality_Neutral_Libertarian
					enviromentalist
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Libertarian_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
	}
	else_if = { limit = { has_country_flag = set_Neutral_green }
		if = { limit = { check_variable = { Neutral_green_leader = 0 } }
			add_to_variable = { Neutral_green_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Godfrey M'Mwereria"
				picture = "generic.dds"
				ideology = Neutral_green
				traits = {
					ethnic_kikuyu
					neutrality_Neutral_green
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_green_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
	}
	else_if = { limit = { has_country_flag = set_Nat_Autocracy }
		if = { limit = { check_variable = { Nat_Autocracy_leader = 0 } }
			add_to_variable = { Nat_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Gen. Daudi Tonje"
				picture = "Gen._Daudi_Tonje.dds"
				ideology = Nat_Autocracy
				traits = {
					ethnic_kalenjin
					military_career
					nationalist_Nat_Autocracy
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Autocracy_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
		if = { limit = { check_variable = { Nat_Autocracy_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Nat_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Gen. Samson Mwathethe"
				picture = "Portrait_Samson_Mwathete.dds"
				ideology = Nat_Autocracy
				traits = {
					ethnic_mijikenda
					military_career
					nationalist_Nat_Autocracy
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Autocracy_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
}