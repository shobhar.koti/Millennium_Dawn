add_namespace = thailand
add_namespace = thailand_news

#Choice Events
#Role of Media
country_event = {
	id = thailand.1
	immediate = { log = "[GetDateText]: [Root.GetName]: event thailand.1" }
	title = thailand.1.t
	desc = thailand.1.d
	picture = GFX_trade_agreement
	
	is_triggered_only = yes
	
	option = {
		name = thailand.1.o1
		log = "[GetDateText]: [This.GetName]: thailand.1.o1 executed"
	}
	
	ai_chance = {
			factor = 1 
	}
	
	option = {
		name = thailand.1.o2
		log = "[GetDateText]: [This.GetName]: thailand.1.o2 executed"
		add_ideas = { SIA_liberalized_media }
	}
	ai_chance = {
			factor = 0 
			modifier = {
				add = 0.25 
				is_historical_focus_on = no 
			}
		}
}