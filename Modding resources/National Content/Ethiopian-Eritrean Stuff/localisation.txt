# english

 ETH_eritrea_category: "Counter-Insurgency Operations in Eritrea"
 ETH_eritrea_category_desc: "Decisions related to anti-terrorism actions. Finding new defensive countermeasures may decrease their max strength but if we want to decrease their current power, we need to develop new and aggressive operations like starting out land offensives and airstrikes.\n\nEPLF's Strength:   §R[?Root.elpf_presence]§!\n(Chance of conducting a new attack)\n\nEPLF's support:  §R[?Root.elpf_support]§!\n(If it reaches 100, the seperatists start a independence war)"

 ETH_destroy_sudan_base: "Destroy The EPLF Base In Sudan"
 ETH_destroy_sudan_base_desc: "Now that we have access to the border region to Eritrea we can send troops in there to end this rebellion once and for all."
 ETH_eritrea_1_category: "Propaganda-Projects in Eritrea"
 ETH_eritrea_1_category_desc: "Decisions related to propaganda projects regarding our acceptance in the annexed regions. A big share of the population still remember the time where Eritrea was part of Ethiopia and they don't call it a good time. Its our job now to convince them that being a part of our great country isn't as bad like before.\n\nAcceptance of Annexation:  §R[?Root.eritrean_acceptance]§!"

# russian

 ETH_eritrea_category: "Операции против повстанцев в Эритрее"
 ETH_eritrea_category_desc: "Решения, связанные с антитеррористическими действиями. Обнаружение новых защитных контрмер может уменьшить их максимальную силу, но если мы хотим уменьшить их текущую мощь, нам необходимо разработать новые и агрессивные операции, такие как начало наземных наступательных операций и авиаударов.\n\nСила EPLF:   §R[?Root.elpf_presence]§!\n(Вероятность проведения новой атаки)\n\nПоддержка EPLF:  §R[?Root.elpf_support]§!\n(Если он достигнет 100, сепаратисты начинают войну за независимость)"
 ETH_destroy_sudan_base: "Уничтожить базу EPLF в Судане"
 ETH_destroy_sudan_base_desc: "Теперь, когда у нас есть доступ к пограничному региону с Эритреей, мы можем послать туда войска, чтобы раз и навсегда положить конец этому восстанию."
 ETH_eritrea_1_category: "Пропагандистские проекты в Эритрее"
 ETH_eritrea_1_category_desc: "Решения, связанные с пропагандистскими проектами относительно нашего принятия в аннексированных регионах. Большая часть населения все еще помнит время, когда Эритрея была частью Эфиопии, и они не называют это хорошим временем. Теперь наша задача убедить их, что быть частью нашей великой страны не так плохо, как раньше.\n\nПринятие аннексии:  §R[?Root.eritrean_acceptance]§!"