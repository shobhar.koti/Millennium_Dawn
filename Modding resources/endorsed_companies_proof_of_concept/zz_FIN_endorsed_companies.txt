ideas = {

	endorsed_company = {

		designer = yes

		FIN_endorsed_company_nokia = {
			allowed = { original_tag = FIN }
			
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FIN_endorsed_company_nokia" }

			cost = 150

			removal_cost = 10

			research_bonus = {
				electronics = 0.15
			}
			
			traits = {
				endorsed_company_mobile_phone_manufacturer
			}

			modifier = {
				production_speed_internet_station_factor = 0.15
				army_org_factor = 0.05
			}
			
			ai_will_do = {
				factor = 1
			}
		}

		FIN_endorsed_company_neste = {
			allowed = { original_tag = FIN }
			
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FIN_endorsed_company_neste" }

			cost = 150

			removal_cost = 10

			research_bonus = {
				Cat_FUEL_OIL = 0.15
				synth_resources = 0.15
			}
			
			traits = {
				endorsed_company_oil_company
			}

			modifier = {
				production_speed_synthetic_refinery_factor = 0.15
				production_speed_fuel_silo_factor = 0.15
				fuel_gain_factor = 0.10
			}
			
			ai_will_do = {
				factor = 1
			}
		}

		FIN_endorsed_company_stora_enso = {
			allowed = { original_tag = FIN }
			
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FIN_endorsed_company_stora_enso" }

			cost = 150

			removal_cost = 10

			research_bonus = {
				industry = 0.15
			}
			
			traits = {
				endorsed_company_logging_company
			}

			modifier = {
				production_speed_industrial_complex_factor = 0.15
				global_building_slots_factor = 0.15
			}
			
			ai_will_do = {
				factor = 1
			}
		}

		FIN_endorsed_company_upm_kymmene = {
			allowed = { original_tag = FIN }
			
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FIN_endorsed_company_upm_kymmene" }

			cost = 150

			removal_cost = 10

			research_bonus = {
				industry = 0.15
				synth_resources = 0.15
			}
			
			traits = {
				endorsed_company_logging_company
			}

			modifier = {
				production_speed_industrial_complex_factor = 0.15
				production_speed_synthetic_refinery_factor = 0.15
			}
			
			ai_will_do = {
				factor = 1
			}
		}

		FIN_endorsed_company_patria = {
			allowed = { original_tag = FIN }
			
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FIN_endorsed_company_patria" }

			cost = 150

			removal_cost = 10

			research_bonus = {
				Cat_AFV = 0.15
			}
			
			traits = {
				endorsed_company_armoured_vehicles
			}

			modifier = {
				supply_consumption_factor = -0.05
			}
			
			ai_will_do = {
				factor = 1
			}
		}
		
	}
	
}
