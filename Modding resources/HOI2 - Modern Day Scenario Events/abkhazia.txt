#########################################################
#							#
# Events for Abkhazia (ALS)                             #             
#                                			#
#########################################################

#########
# Index #
#########################################################
#From	- To	#	Name 			 
#########################################################
#94001 - 94300# Main events
#94301 - 94450# Political events
#94451 - 94500# Election events	 		
#########################################################

############################################################################################################################

#############################################
###### Main events
#############################################

#############################################
###### Joining Russia?
#############################################
event = {
         id = 94001
         random = no
         country = ALS
         trigger = {
		   atwar = no
		   event = 94520
		   NOT = { 
			OR = {
				puppet = { country = ALS country = GEO }
				war = { country = RUS country = USA }
				atwar = no
			}
                   }

         }
 
         name = "EVT_94001_NAME"
         desc = "EVT_94001_DESC"
         style = 0
	 picture = "ossetia_independent"
 
         date = { day = 5 month = december year = 2008 }
 
           action_a = {
                  name = "We want to be a part of Russia"
		  ai_chance = 99
		  command = { type = trigger which = 94002 }
           }
	   action_b = {
                  name = "Abkhazia will remain independent"
		  ai_chance = 1
		  command = { type = dissent value = 10 }
		  command = { type = sleepevent which = 94521 }
           }

}
#############################################
###### Abkhazia wants to become a part of Russia
#############################################
event = {
         id = 94002
         random = no
         country = RUS
 
         name = "EVT_94002_NAME"
         desc = "EVT_94002_DESC"
         style = 0
	 picture = "abkhazia"
 
           action_a = {
                  name = "We accept them"
		  ai_chance = 99
		  command = { type = setflag which = rus_annex_cauc2 }
		  command = { type = trigger which = 94003 }
		  command = { type = trigger which = 94004 }
           }
	   action_b = {
                  name = "We don't need them"
		  ai_chance = 1
		  command = { type = relation which = ALS value = -10 }
		  command = { type = sleepevent which = 94521 }
           }

}
#############################################
###### Russia agrees
#############################################
event = {
         id = 94003
         random = no
         country = RUS
 
         name = "EVT_94003_NAME"
         desc = "EVT_94003_DESC"
         style = 0
	 picture = "no"
 
           action_a = {
                  name = "Ok"
		  command = { }
           }

}
#############################################
###### Russia agrees
#############################################
event = {
         id = 94004
         random = no
         country = ALS
 
         name = "EVT_94004_NAME"
         desc = "EVT_94004_DESC"
         style = 0
	 picture = "no"
 
           action_a = {
                  name = "Ok"
		  command = { }
           }

}